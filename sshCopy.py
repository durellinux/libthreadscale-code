import os
import sys

DEST = "beastie"
FOLDER_SRC = "~/libQoS"
FOLDER_BUILD = "~/libQoS/src-build"
CP_CMD = "rsync -Pt"

def reverseScp(src, dest):
	cmd = CP_CMD + " -r "+DEST+":"+src+ " "+dest
	os.system(cmd)

def scp(src, dest):
	cmd = CP_CMD + " -r "+src+" "+DEST+":"+dest
	os.system(cmd)

def ssh(command):
	cmd = "ssh "+DEST+" "+"\""+command+"\""
	print cmd
	os.system(cmd)

def system(cmd):
	os.system(cmd)

def makeCallBack():
	ssh("cd "+FOLDER_BUILD + " && make")

def copyCallBack():
	# ssh("mkdir -p "+FOLDER_SRC)
	scp(FOLDER + "/*", FOLDER_SRC)

def cmakeCallBack():
	ssh("mkdir -p "+FOLDER_BUILD)
	# ssh("python /bgusr/home2/gcdurell/work/CNK-BGAS/cmakeBGQ.py")
	ssh("cd "+FOLDER_BUILD + " && cmake " + FOLDER_SRC + "/")


options = ["make", "cmake", "copy"]
callbacks = [makeCallBack, cmakeCallBack, copyCallBack]

if len(sys.argv)<2:
	print "USAGE: python sshCopy.py CMDS ..."
	print "CMDS may assume the following values: "
	print "\n".join(options)
	print "The first options can be the name of the machine to connect to"
	sys.exit(0)

if sys.argv[1] not in options:
	DEST = str(sys.argv[1])
	argList = sys.argv[2:]
else:
	argList = sys.argv[1:]

if argList[0] not in options:
	FOLDER = str(argList[0])
	argList = argList[1:]
else:
	FOLDER = "src"
	argList = argList[0:]

FOLDER_SRC = FOLDER_SRC + os.sep + FOLDER	

for opt in argList:
	if opt not in options:
		print opt + " is not a valid command... Valid options are: "
		print "\n".join(options)
		sys.exit(0)

	callbacks[options.index(opt)]()
