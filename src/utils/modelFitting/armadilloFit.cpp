#include "armadilloFit.hpp"
#include <iostream>
#include <armadillo>

using namespace arma;

double mse(vec x, vec y, vec model){
    int degree = model.size();
    int observations = x.size();

    double mse = 0;

    for(int i=0; i<observations; i++){
        double val = 0;
        for(int d=0; d<degree; d++){
            val += pow(x(i), d) * model(i);
        }
        mse += pow(y(i) - val, 2);
    }

    return mse;
}

bool fit(TYPE *x, TYPE *y, TYPE *v, int *degree, unsigned int observations){
    if(observations <= 3){
        *degree = 0;
        return false;
    }

    int maxDegree = 4;

    std::vector<TYPE> bValues;
    std::vector<TYPE> aValues;


    for(int i=0; i<observations; i++){
        if(std::find(bValues.begin(), bValues.end(), y[i]) == bValues.end()){
//            std::cout << y[i] << std::endl;
            bValues.push_back(y[i]);
        }

        if(std::find(aValues.begin(), aValues.end(), x[i]) == aValues.end()){
            aValues.push_back(x[i]);
        }
    }

    *degree = observations - 1;
    if(*degree > maxDegree)
        *degree = maxDegree;

    if(bValues.size() < *degree)
        *degree = bValues.size();

    if(aValues.size() < *degree)
        *degree = aValues.size();


    vec b;
    mat A;
    vec res;

    b.resize(observations);

    for(int i=0; i<observations; i++){
        b(i) = y[i];
    }

    std::cout << "Fitting using " << *degree << "variables" << std::endl;

    A.resize(observations, *degree);

    for(int i=0; i<observations; i++){
        for(int d=0; d<*degree; d++){
            A(i, d) = pow(x[i], d);
        }
    }

    bool solved = solve(res, A, b);
//    solve(res, A, b);

    if(solved==true)
        std::cout << res << std::endl;

    return solved;

}
