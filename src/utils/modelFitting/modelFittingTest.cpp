#include "modelFitting.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <armadillo>
#include "armadilloFit.hpp"

#define OBS 5
#define VAR 2

//using namespace arma;

int main(){
    TYPE x[OBS], y0[OBS], y1[OBS], y2[OBS], y3[OBS], model[VAR];

//    vec model;

    x[0] = 4;
    x[1] = 4;
    x[2] = 4;
    x[3] = 4;
    x[4] = 4;

    y0[0] = 5;
    y0[1] = 10;
    y0[2] = 15;
    y0[3] = 20;
    y0[4] = 25;

    y1[0] = 4;
    y1[1] = 15;
    y1[2] = 1;
    y1[3] = 14;
    y1[4] = 5;

    y2[0] = 4;
    y2[1] = 4;
    y2[2] = 4;
    y2[3] = 4;
    y2[4] = 4;

    y3[0] = 1;
    y3[1] = 2;
    y3[2] = 3;
    y3[3] = 4;
    y3[4] = 4;

    struct timeval t0, t1;
    gettimeofday(&t0, NULL);

    int d = 0;

//    fit2(x, y0, model, OBS);
    fit(x, y0, model, &d, OBS);
    fit(x, y1, model, &d, OBS);
    fit(x, y2, model, &d, OBS);
    fit(x, y3, model, &d, OBS);

    gettimeofday(&t1, NULL);

//    printf("Time %f\n", t1.tv_sec*1.0 - t0.tv_sec + (t1.tv_usec - t0.tv_usec)*1.0/1000000);
}
