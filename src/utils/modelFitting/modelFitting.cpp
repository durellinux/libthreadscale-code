#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include "modelFitting.hpp"

static inline void createAquadratic(TYPE *x, TYPE *m, TYPE *mt, unsigned int observations){
    int i;

    for(i=0;i<observations;i++){
		TYPE v1 = x[i];
		TYPE v2 = v1*v1;
 
        mt[0*observations + i] = v2;
        mt[1*observations + i] = v1;
		
        m[i*2+0] = v2;
        m[i*2+1] = v1;
	}
}


static inline void createAlinear(TYPE *x, TYPE *m, TYPE *mt, unsigned int observations){
    int i;

    for(i=0;i<observations;i++){
        TYPE v1 = 1;
        TYPE v2 = x[i];

        mt[0*observations + i] = v2;
        mt[1*observations + i] = v1;

        m[i*2+0] = v2;
        m[i*2+1] = v1;
    }
}


static inline void multiplyMat(TYPE *a, TYPE *b, TYPE *c, int row1, int col1, int col2){
	int i,j,k;
	TYPE sum;
	
	for(i=0; i<row1; i++){
		for(j=0; j<col2; j++){
			sum = 0;
			for(k=0; k<col1; k++)
				sum += a[i*col1 + k] * b[k*col2 + j];
			
			c[i*col2+j] = sum;
		}
	}
}

static void inline invert2x2(TYPE *m, TYPE *mi){
    TYPE det = m[0*2+0]*m[1*2+1] - m[0*2+1]*m[1*2+0];

    if(det!=0){
//        printf("Det != 0\t%f\n", det);
        mi[0*2+0] = m[1*2+1]/det;
        mi[0*2+1] = -m[1*2+0]/det;
        mi[1*2+0] = -m[0*2+1]/det;
        mi[1*2+1] = m[0*2+0]/det;
    }
    else{
//        printf("Det == 0\t%f\n", det);
        memset(mi, 0, sizeof(TYPE) * 2 * 2);
    }

}

static TYPE inline mseLinear(TYPE *x, TYPE* y, TYPE *params, unsigned int observations){
    TYPE mse = 0.0;

    for(int i=0; i<observations; i++){
        TYPE diff = y[i] - (params[0]*x[i] + params[1]);
        mse += diff * diff;
    }

    return mse;
}

static TYPE inline mseQuadratic(TYPE *x, TYPE* y, TYPE *params, unsigned int observations){
    TYPE mse = 0.0;

    for(int i=0; i<observations; i++){
        TYPE diff = x[i] - (params[0]*y[i]*y[i] + params[1]*y[i]);
        mse += diff * diff;
    }

    return mse;
}

Model fit2(TYPE *x, TYPE *y, TYPE *v, unsigned int observations){
    TYPE *A, *At, *t0, *t0i, *t1, *linearFit, *quadraticFit;
    TYPE mseLin, mseQuad;
    Model retValue;

    A = (TYPE *) malloc(observations * 2 * sizeof(TYPE));    // obs x 2
    At = (TYPE *) malloc(observations * 2 * sizeof(TYPE));   // 2 x obs
    t0 = (TYPE *) malloc(2 * 2 * sizeof(TYPE));              // 2 x 2
    t0i = (TYPE *) malloc(observations * 2 * sizeof(TYPE));  // 2 x 2
    t1 = (TYPE *) malloc(observations * 2 * sizeof(TYPE));   // 2 x obs
    linearFit = (TYPE *) malloc(2 * sizeof(TYPE));           // 2 x 1
    quadraticFit = (TYPE *) malloc(2 * sizeof(TYPE));        // 2 x 1

    createAlinear(x, A, At, observations);
    multiplyMat(At, A, t0, 2,observations, 2);
    invert2x2(t0, t0i);
    multiplyMat(t0i, At, t1, 2, 2, observations);
    multiplyMat(t1, y, linearFit, 2, observations, 1);

    createAquadratic(y, A, At, observations);
    multiplyMat(At, A, t0, 2,observations, 2);
    invert2x2(t0, t0i);
    multiplyMat(t0i, At, t1, 2, 2, observations);
    multiplyMat(t1, x, quadraticFit, 2, observations, 1);

    mseLin = mseLinear(x, y, linearFit, observations);
    mseQuad = mseQuadratic(x, y, quadraticFit, observations);

    retValue = mseLin <= mseQuad ? LINEAR : QUADRATIC;
    if(retValue == LINEAR)
        memcpy(v, linearFit, 2);
    else
        memcpy(v, quadraticFit, 2);

//    printf("Linear\t%f\t%f\t%f\t%u\n", linearFit[0], linearFit[1], mseLin, (unsigned int)retValue);
//    printf("Quadratic\t%f\t%f\t%f\t%u\n", quadraticFit[0], quadraticFit[1], mseQuad, (unsigned int)retValue);

    free(A);
    free(At);
    free(t0);
    free(t0i);
    free(t1);
    free(linearFit);
    free(quadraticFit);

    return retValue;
}
