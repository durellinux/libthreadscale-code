#ifndef MODELFITTING_HPP
#define MODELFITTING_HPP

typedef float TYPE;

typedef enum{LINEAR=0, QUADRATIC} Model;

Model fit2(TYPE *x, TYPE *y, TYPE *v, unsigned int observations);

#endif
