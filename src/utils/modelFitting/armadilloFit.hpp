#ifndef ARMADILLOFIT_HPP
#define ARMADILLOFIT_HPP

typedef float TYPE;

bool fit(TYPE *x, TYPE *y, TYPE *v, int *degree, unsigned int observations);

#endif
