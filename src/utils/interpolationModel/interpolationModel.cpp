#include "interpolationModel.hpp"


#include "assert.h"
#include <iostream>
#include <algorithm>
#include <utils.hpp>

void InterpolationModel::addToReport()
{
    reportModel.push_back(model);
}

void InterpolationModel::createModel(std::map<int, double> &points, std::pair<int, int> range)
{

    model.clear();

    // If no hisotry point => do nothing
    if(points.size() == 0)
        return;

    std::vector<InterpolationSupport> support;
    std::pair<int, double> prev(0, 0.0);

    for(auto &p : points){
        double m = (prev.second - p.second) / (prev.first - p.first);
        InterpolationSupport sup;
        sup.start = prev.first;
        sup.end = p.first;
        sup.tan = m;
        support.push_back(sup);
        prev.first = p.first;
        prev.second = p.second;

//        std::cout << "STEP 1 : " << sup.start << " - " << sup.end << " - " << sup.tan << std::endl;
    }

    auto itSup = support.end() - 1;
    if(prev.first != range.second){
        InterpolationSupport sup;
        sup.start = prev.first;
        sup.end = range.second;
        sup.tan = itSup->tan;

        support.push_back(sup);

//        std::cout << "STEP 2 : " << sup.start << " - " << sup.end << " - " << sup.tan << std::endl;
    }

    model[0] = 0.0;

    for(int i=range.first; i<=range.second; i++){

        auto findTan = [&] (InterpolationSupport sup) {
            return i>=sup.start && i<=sup.end;
        };

        auto vals = std::find_if(support.begin(), support.end(), findTan);

        model[i] = model[vals->start] + (i - vals->start) * vals->tan;
//        std::cout << i << " - " << vals->start << " - " << vals->end << std::endl;
    }

//    std::cout << model.size() << std::endl;

    assert(model.size() == (range.second - range.first + 2));

    addToReport();

}

double InterpolationModel::getValue(int x)
{
//    std::cout << "Model size: " << model.size() << std::endl;

    if(model.size() == 0)
        return -1;

    assert(model.find(x)!=model.end());
    return model[x];
}

int InterpolationModel::getResources(double QoS)
{
    int max = getAvailableProcessors();

    for(auto &p : model){
        max = p.first;
        if(p.second > QoS)
            return p.first;
    }

    return max;
}

std::vector<std::string> InterpolationModel::dumpModels()
{
    std::vector<std::string> output;
    for(auto &m : reportModel){
        std::string s = "MODELS\t";
        for(auto &v : m){
            s += MAKE_STRING(v.second) + "\t";
        }
        output.push_back(s);
    }

    return output;
}
