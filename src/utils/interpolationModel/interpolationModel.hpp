#ifndef INTERPOLATION_MODEL
#define INTERPOLATION_MODEL

#include <vector>
#include <map>

typedef struct{
    int start;
    int end;
    double tan;
} InterpolationSupport;

class InterpolationModel{
    std::map<int, double> model;

    std::vector<std::map<int, double>> reportModel;

    void addToReport();

public:
//    InterpolationModel();
//    ~InterpolationModel();

    void createModel(std::map<int, double> &points, std::pair<int, int> range);

    double getValue(int x);
    int getResources(double QoS);

    std::vector<std::string> dumpModels();

};

#endif
