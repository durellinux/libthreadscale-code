#include "utils.hpp"
#include <unistd.h>
#include <sys/time.h>
#include <sys/time.h>

#include <assert.h>
#include <iostream>

std::vector<OverheadReport> overheadReport;

unsigned int getAvailableProcessors(){
    return sysconf(_SC_NPROCESSORS_ONLN);
}

uint64_t getTime(){
    struct timeval tv;

    gettimeofday(&tv, NULL);

    return tv.tv_sec * 1000 * 1000 + tv.tv_usec;
}

double exponentialMovingAverage(std::vector<double> values, double alpha){

    assert(values.size() > 0);

    double avg = values[0];

    for(int i=1; i<values.size(); i++){
        avg = alpha * values[i] + (1 - alpha) * avg;
    }

    return avg;
}

void addOverheadEvent(uint64_t start, uint64_t stop, std::string event){

    OverheadReport o;
    o.start = start;
    o.end = stop;
    o.event = event;
    overheadReport.push_back(o);
}

std::vector<std::string> dumpOverheads(){
    std::vector<std::string> output;
    for(auto &o : overheadReport){
        std::string s = "OVERHEADS\t"  + MAKE_STRING(o.event) + "\t" + MAKE_STRING(o.end - o.start);
        output.push_back(s);
    }
    return output;
}

//template<typename T> void dumpVector(std::vector<T> v, int startIndex, int endIndex){
void dumpVector(std::vector<double> v, int startIndex, int endIndex){
    for(int i=startIndex; i<endIndex; i++)
        std::cout << v[i] << " ";

    std::cout << std::endl;
}
