#ifndef UTILS
#define UTILS

#include <inttypes.h>
#include <vector>
#include <string>
#include <sstream>

//utility macro for making a string
#ifdef MAKE_STRING
#undef MAKE_STRING
#endif
//#define MAKE_STRING( msg )  ( ((std::ostringstream&)((std::ostringstream() << '\x0') << std::setprecision(9) << msg)).str().substr(1) )
#define MAKE_STRING( msg )  ( ((std::ostringstream&)((std::ostringstream() << '\x0') << msg)).str().substr(1) )

// Blocking clCreateBuffer
#define blockingClCreateBuffer(context, accessType, memorySize, hostPointer, returnPointer) do{clCreateBuffer(context, accessType, memorySize, hostPointer, returnPointer)}while(returnPointer!=NULL);

//clCreateBuffer(openCLInfo.context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR ,sizeof(cl_float) * globalArgs.numberOfPixels * globalArgs.numberOfSuccFrame, inputMatrix, &ret);


typedef struct{
    uint64_t start;
    uint64_t end;
    std::string event;
} OverheadReport;

extern std::vector<OverheadReport> overheadReport;

unsigned int getAvailableProcessors();
uint64_t getTime();

double exponentialMovingAverage(std::vector<double> values, double alpha);
void addOverheadEvent(uint64_t start, uint64_t stop, std::string event);
std::vector<std::string> dumpOverheads();

//template<typename T> void dumpVector(std::vector<T> v, int startIndex, int endIndex);
void dumpVector(std::vector<double> v, int startIndex, int endIndex);

#endif
