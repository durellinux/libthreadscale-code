#include "libThreadScale.hpp"
#include <utils.hpp>
#include <iostream>
#include <fstream>

#ifdef _OPENMP
#include <omp.h>
#endif

const double coreCost = 1.3;
const double coreDiscount = 0.975;
std::map<unsigned int, double> coresToMoney;

void createCostModel(){
    unsigned int totalCores = getAvailableProcessors();

    for(unsigned int i = 1; i<=totalCores; i++)
        coresToMoney[i] = coreCost * i;
}

int LibThreadScale::getThreadsToUse()
{
    threadsRequests++;
    computeThreadsTouse();

    lastThreadRequest = getTime();
    lastThreadResponse = threadsToUse;

    PerformanceReport p;
    p.start = lastThreadRequest;
    p.threads = lastThreadResponse;

    performanceReport.push_back(p);

    #ifdef _OPENMP
//        std::cout << "Forcing to use " << lastThreadResponse << " threads using openmp" << std::endl;
        omp_set_dynamic(0);     // Explicitly disable dynamic teams
        omp_set_num_threads(lastThreadResponse); // Use 4 threads for all consecutive parallel regions
    #endif

    return lastThreadResponse;
}

void LibThreadScale::sendHeartbeat(double heartbeats)
{
    uint64_t start = getTime();

    uint64_t heartbeatTime = getTime();
    double time = heartbeatTime - lastThreadRequest;

    int threads = lastThreadResponse;

    threadPerformanceHistory[threads].push_back(heartbeats/time  * 1000 * 1000);
    std::vector<int> toErase;

    for(auto &p : threadPerformanceHistory){
        if(p.second.size()>0 && (threadsRequests % forgetAfterRequests == 0)){
            p.second.erase(p.second.begin());
            if(p.second.size() == 0)
                toErase.push_back(p.first);
        }
    }

    for(auto &e : toErase)
        threadPerformanceHistory.erase(e);

    monitor.addPerformanceData(heartbeatTime, heartbeats);

    auto p = performanceReport.end() - 1;
    p->stop = heartbeatTime;
    p->instantPerf = monitor.getValue(INSTANT_THROUGHPUT);
    p->windowPerf = monitor.getValue(WINDOW_THROUGHPUT);
    p->globalPerf = monitor.getValue(GLOBAL_THROUGHPUT);

#ifdef _OPENMP
    getThreadsToUse();
#endif

    uint64_t end = getTime();
    addOverheadEvent(start, end, "SEND_HB");
}

void LibThreadScale::computeThreadsSimple()
{
    double curQoS = monitor.getValue(GLOBAL_THROUGHPUT);

    if(curQoS!=0){
        if(curQoS < minQoS)
            threadsToUse = lastThreadResponse + 1;
        else if(curQoS > minQoS * 1.1)
            threadsToUse = lastThreadResponse - 1;
    }
    else
        threadsToUse = getAvailableProcessors();
}

void LibThreadScale::computeThreadsInterpolation()
{
    updateModel();
    double curPerf = monitor.getValue(WINDOW_THROUGHPUT);
    double speedup = minQoS / curPerf;

    threadsToUse = model.getResources(minQoS * speedup);

    double estimatedSpeedup = model.getValue(threadsToUse);
    if(estimatedSpeedup != -1){
        unsigned int suggestedThreads = threadsToUse;
        double costEfficiency = coresToMoney[threadsToUse] / model.getValue(threadsToUse);

        for(unsigned int i = threadsToUse+1; i<getAvailableProcessors(); i++){
            double tmp = coresToMoney[i] / model.getValue(i);
            std::cout << "Checking thread: " << i << " - " << costEfficiency << " / " << tmp << std::endl;
            if(tmp > costEfficiency){
                threadsToUse = i;
                costEfficiency = tmp;
            }
        }
        std::cout << "Suggested threads: " << suggestedThreads << " - Used thread: " << threadsToUse << std::endl;
    }

//    std::cout << "Threads " << threadsToUse << std::endl;
}

void LibThreadScale::computeThreadsTouse()
{
    uint64_t start = getTime();

    computeThreadsInterpolation();

    if(threadsToUse == 0)
        threadsToUse = 1;
    if(threadsToUse>maxThreads)
        threadsToUse = maxThreads;

    if(forceThreads!=0)
        threadsToUse = forceThreads;

    uint64_t end = getTime();
    addOverheadEvent(start, end, "GET_THREADS");
}

void LibThreadScale::updateModel()
{
    uint64_t start = getTime();

    std::map<int, double> tempThreadToPerformance;

    for(auto &v : threadPerformanceHistory){
        tempThreadToPerformance[v.first] = exponentialMovingAverage(v.second, 0.98); // TODO: Check alpha value
    }

    model.createModel(tempThreadToPerformance, std::pair<int, int>(1, maxThreads));

    uint64_t stop = getTime();

    addOverheadEvent(start, stop, "UPDATE_MODEL");
}

void LibThreadScale::debugInfo()
{
    std::cout << "Avg. performance: " << monitor.getValue(GLOBAL_THROUGHPUT) << " - " << lastThreadResponse <<  std::endl;
}

void LibThreadScale::dumpInfo(char *outputPrefix)
{
    std::vector<std::string> models = model.dumpModels();
    std::vector<std::string> overheads = dumpOverheads();
    std::vector<std::string> perfReport = dumpPerformanceReport();

    std::string s(outputPrefix);

    std::ofstream modelFile, overheadFile, perfReportFile;
    modelFile.open((s + "_Model.dat").c_str());
    overheadFile.open((s + "_Overhead.dat").c_str());
    perfReportFile.open((s + "_PerfReport.dat").c_str());

    for(auto &r : models)
        modelFile << r << std::endl;

    for(auto &r : overheads)
        overheadFile << r << std::endl;

    for(auto &r : perfReport)
        perfReportFile << r << std::endl;

    modelFile.close();
    overheadFile.close();
    perfReportFile.close();


    for(auto p : coresToMoney){
        unsigned int cores = p.first;
        double cost = p.second;
        double thr = model.getValue(cores);
        double efficiency = thr / cost;
        std::cout << cores << " : " << cost << " - " << thr << " - " << efficiency << std::endl;
    }
}

std::vector<std::string>  LibThreadScale::dumpPerformanceReport()
{
    std::vector<std::string> output;
    for(auto &p : performanceReport){
        std::string s = "PERF_REPORT\t" + MAKE_STRING(p.start) + "\t" + MAKE_STRING(p.stop) + "\t"
                + MAKE_STRING(p.stop - p.start) + "\t" + MAKE_STRING(p.threads) + "\t" + MAKE_STRING(p.instantPerf)
                + "\t" + MAKE_STRING(p.windowPerf) + "\t" + MAKE_STRING(p.globalPerf) + "\t" + MAKE_STRING(coresToMoney[p.threads]);
        output.push_back(s);
    }
    return output;
}

LibThreadScale::LibThreadScale()
{
    this->maxThreads = getAvailableProcessors();
    this->maxPerformanceHistoryPoints = 10;
    this->threadsRequests = 0;
    this->forgetAfterRequests = 5;
    this->minQoS = 0;

    this->lastThreadResponse = getAvailableProcessors();
    this->threadsToUse = 0;

    createCostModel();

#ifdef _OPENMP
    getThreadsToUse();
#endif
}

LibThreadScale::~LibThreadScale()
{

}

void LibThreadScale::setMinQoS(double qos)
{
    this->minQoS = qos;
}

void LibThreadScale::setForceThreads(unsigned int threads){
    this->forceThreads = threads;
}
