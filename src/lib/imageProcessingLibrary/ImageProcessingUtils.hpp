#ifndef _IMAGE_PROCESSING_H_
#define _IMAGE_PROCESSING_H_

#include <string>
#include <sstream>

#define PATH_SEP "/"

#ifdef MAKE_STRING
#undef MAKE_STRING
#endif
#define MAKE_STRING( msg )  ( ((std::ostringstream&)((std::ostringstream() << '\x0') << msg)).str().substr(1) )



typedef struct {
  int rows;
  int cols;
  int depth;
  unsigned char* header;
  unsigned char* data;
} sImage;

void initImage(sImage* image, int rows, int cols, int depth, unsigned char* headar);
void deleteImage(sImage* image);
void readImage(const char* filename, sImage* image);
void writeImage(const char* filename, sImage* image);

void rgb2grey (sImage* originalImage, sImage* greyImage);
void grey2rgb (sImage* originalImage, sImage* edgeImage);

#endif
