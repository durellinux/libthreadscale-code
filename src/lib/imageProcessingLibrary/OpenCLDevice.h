//
//  ImageProcessingImplementation.h
//  ImageProcessingApp1
//
//  Created by Raegal on 25/03/15.
//  Copyright (c) 2015 ingconti.com. All rights reserved.
//

#ifndef __OpenCLDevice__
#define __OpenCLDevice__

#include <unistd.h>
#include <time.h>
#include <functional>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <vector>
#include <map>
#include <string>

#ifdef __APPLE__
#include "OpenCL/opencl.h"
#else
#include "CL/cl.h"
#endif


#define MAX_VECTORSIZE 16
#define RGB2GREY_VECTORSIZE 4
#define MOTION_DETECTION_VECTORSIZE 16

//round up globalSize to the nearest multiple of baseSize
#define ROUNDUP(baseSize,globalSize)   ((globalSize%baseSize==0) ? (globalSize) : (globalSize + baseSize - globalSize%baseSize))

#define MAX_SOURCE_SIZE (0x100000)

#define NVIDIA_GPU 0
#define ARM_GPU 1

#ifndef TARGET_PLATFORM
#define TARGET_PLATFORM NVIDIA_GPU
#endif

typedef std::function<void(void)> KernelCompletionBlockType;
typedef std::function<void(void)> KernelExectionBlockType;


typedef struct {
    cl_kernel kernel;
    KernelExectionBlockType executionBlock;
    KernelCompletionBlockType completionBlock;
} KernelInfo;

class OpenCLDevice {
    
    
public:
    
    cl_platform_id platform_id = NULL;
    cl_device_id device_id = NULL;
    cl_context context = NULL;
    cl_command_queue command_queue = NULL;
    cl_program program = NULL;
    cl_uint ret_num_devices;
    cl_uint ret_num_platforms;
    cl_uint max_compute_units;
    
    size_t max_global_work_size;
    size_t max_work_item_size_dimension[3];
    
    char clVersion[128];
    
    size_t source_size;
    char *source_str;
    
    std::map<std::string,cl_kernel> kernels;
    cl_device_type clType;
    
    OpenCLDevice();
    OpenCLDevice(cl_device_type dType);
    ~OpenCLDevice();
    
    cl_int addKernel(std::string kernelKey);
    void executeKernel(std::string kernelKey, KernelExectionBlockType executionBlock, KernelCompletionBlockType completionBlock);
    cl_int removeKernel(std::string kernelKey);
    
    

};


void print_app();
void initOpenCLDevice(cl_device_type dType);
void destroyOpenCLDevice();




#endif
