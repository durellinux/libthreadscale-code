#include <map>
#include <iostream>
#include <libThreadScale.hpp>

std::map<std::string, LibThreadScale *> kernelsMap;

extern "C"
void debugstring_(char *kernelName, int ll){
    kernelName[ll-1] = '\0';
    std::string kernel(kernelName);

    std::cout << "String : " << kernel << std::endl;
}

LibThreadScale *getFromMap(char *kernelName, int ll){
    kernelName[ll-1] = '\0';
    std::string kernel(kernelName);

//    std::cout << "Message from " << kernel << std::endl;

    if(kernelsMap.find(kernel) == kernelsMap.end()){
        LibThreadScale *l = new LibThreadScale();
        kernelsMap[kernel] = l;
    }

//    std::cout << "Kernel creation done" << std::endl;

    return kernelsMap[kernel];
}

void removeFromMap(char *kernelName, int ll){
    kernelName[ll-1] = '\0';
    std::string kernel(kernelName);

    if(kernelsMap.find(kernel) == kernelsMap.end()){
        LibThreadScale *l = kernelsMap[kernel];
        delete l;
        kernelsMap.erase(kernel);
    }
}

extern "C"
void newthreadscale_(char *kernelName, int ll){
    getFromMap(kernelName, ll);
}

extern "C"
void destroythreadscale_(char *kernelName, int ll){
    removeFromMap(kernelName, ll);
}

extern "C"
int getthreadstouse_(char *kernelName, int ll){
    LibThreadScale *l = getFromMap(kernelName, ll);
    return l->getThreadsToUse();
}

extern "C"
void setminqos_(double *minQoS, char *kernelName, int ll){
    LibThreadScale *l = getFromMap(kernelName, ll);
    l->setMinQoS(*minQoS);
}

extern "C"
int sendheartbeat_(double *heartbeats, char *kernelName, int ll){
    LibThreadScale *l = getFromMap(kernelName, ll);
//    std::cout << *heartbeats << std::endl;
    l->sendHeartbeat(*heartbeats);
    return 0;
}

extern "C"
int sendheartbeatfull_(char *kernelName, double *minQoS, double *heartbeats, int ll){
    LibThreadScale *l = getFromMap(kernelName, ll);
    l->setMinQoS(*minQoS);
    l->getThreadsToUse();
    l->sendHeartbeat(*heartbeats);

    return l->getThreadsToUse();
}

extern "C"
void dumpinfo_(char *kernelName, char *suffix, int kl, int sl){
    LibThreadScale *l = getFromMap(kernelName, kl);
    for(int i=0; i<sl; i++)
        if(suffix[i] == 32)
            suffix[i] = '\0';
    suffix[sl-1] = '\0';
    l->dumpInfo(suffix);
}

