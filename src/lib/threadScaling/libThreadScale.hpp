#ifndef LIBTHREADSCALE
#define LIBTHREADSCALE

#include <map>
#include <vector>

#include <monitor.hpp>
#include <inttypes.h>

#include <interpolationModel.hpp>
#include <utils.hpp>


typedef struct{
    uint64_t start;
    uint64_t stop;
    unsigned int threads;
    double instantPerf;
    double windowPerf;
    double globalPerf;
} PerformanceReport;

class LibThreadScale{

    uint64_t threadsRequests;

    int maxThreads;
    int maxPerformanceHistoryPoints;
    int forgetAfterRequests;

    int forceThreads;

    int threadsToUse;
    std::map<int, std::vector<double>> threadPerformanceHistory;
    std::vector<double> performanceWindow;

    InterpolationModel model;

    double minQoS;

    Monitor monitor;

    uint64_t lastThreadRequest;
    int lastThreadResponse;

    std::vector<PerformanceReport> performanceReport;

    void computeThreadsSimple();
    void computeThreadsInterpolation();

    void computeThreadsTouse();
    void updateModel();



public:
    LibThreadScale();

    ~LibThreadScale();

    int getThreadsToUse();

    void setMinQoS(double qos);
    void setForceThreads(unsigned int threads);
    void sendHeartbeat(double heartbeats);

    void debugInfo();

    void dumpInfo(char *outputPrefix);
    std::vector<std::string> dumpPerformanceReport();
};



#endif
