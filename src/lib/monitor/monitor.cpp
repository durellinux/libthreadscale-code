#include "monitor.hpp"

#include "assert.h"
#include <utils.hpp>

#include <iostream>

void Monitor::updatePerformanceInfo(uint64_t time, double heartbeats)
{
    monitoredQuantities[TOTAL_HEARTBEATS] += heartbeats;
    monitoredQuantities[INSTANT_THROUGHPUT] = heartbeats / (time - lastTime) * 1000 * 1000;
    monitoredQuantities[WINDOW_THROUGHPUT] = computeWindowThroughput();
    monitoredQuantities[GLOBAL_THROUGHPUT] = monitoredQuantities[TOTAL_HEARTBEATS] / (time - startTime) * 1000 * 1000;
    monitoredQuantities[KERNEL_ITERATIONS] += 1;
    monitoredQuantities[AVERAGE_HEARTBEATS] = monitoredQuantities[TOTAL_HEARTBEATS] / monitoredQuantities[KERNEL_ITERATIONS];

//    std::cout << "Throughput " << monitoredQuantities[INSTANT_THROUGHPUT] << "\t" << monitoredQuantities[WINDOW_THROUGHPUT] << std::endl;
//    std::vector<double> perfVector = getWindowPerformance();
//    dumpVector(perfVector, 0, perfVector.size());

}

double Monitor::computeWindowThroughput()
{
    double sum = 0;
    double time = windowStartTime;
//    bool first = true;
    for(auto &p : performanceMonitoring){
//        if(!first)
            sum += p.second / (p.first - time) * 1000 * 1000;
//        first = false;
        time = p.first;
    }

    return sum / (performanceMonitoring.size());
}

Monitor::Monitor()
{
    maxPerformanceData = 10;

    startTime = getTime();
    lastTime = startTime;
    windowStartTime = getTime();

    monitoredQuantities[TOTAL_HEARTBEATS] = 0.0;
    monitoredQuantities[INSTANT_THROUGHPUT] = 0.0;
    monitoredQuantities[WINDOW_THROUGHPUT] = 0.0;
    monitoredQuantities[GLOBAL_THROUGHPUT] = 0.0;
    monitoredQuantities[KERNEL_ITERATIONS] = 0.0;
    monitoredQuantities[AVERAGE_HEARTBEATS] = 0.0;
}

void Monitor::addPerformanceData(uint64_t time, double heartbeats)
{
    std::pair<uint64_t, double> p(time, heartbeats);
    performanceMonitoring.push_back(p);

    if(performanceMonitoring.size() > maxPerformanceData){
//        std::cout << "Old starttime: " << (windowStartTime - startTime) << " - New: " << (performanceMonitoring[0].first - startTime) << std::endl;
        windowStartTime = performanceMonitoring[0].first;

        performanceMonitoring.erase(performanceMonitoring.begin());
    }

    updatePerformanceInfo(time, heartbeats);

    lastTime = time;
}

double Monitor::getValue(std::string parameter)
{
    assert(monitoredQuantities.find(parameter) != monitoredQuantities.end());

    return monitoredQuantities[parameter];
}

unsigned int Monitor::getWindowLength()
{
    return this->performanceMonitoring.size();
}

uint64_t Monitor::getStartTime()
{
    return startTime;
}

std::vector<double> Monitor::getWindowPerformance()
{
    std::vector<double> v;
    uint64_t time = windowStartTime;
    for(auto p: performanceMonitoring){
        v.push_back(p.second/(p.first - time) * 1000 * 1000);
        time = p.first;
    }

    return v;
}


