#ifndef MONITOR
#define MONITOR

#include <vector>
#include <map>
#include <string>

#define TOTAL_HEARTBEATS "Total HeartBeats"
#define INSTANT_THROUGHPUT "Instant Throughput"
#define WINDOW_THROUGHPUT "Window Througput"
#define GLOBAL_THROUGHPUT "Global Throughput"
#define KERNEL_ITERATIONS "Iterations"
#define AVERAGE_HEARTBEATS "Average Heartbeats"

class Monitor{

    int maxPerformanceData;

    uint64_t startTime;
    uint64_t windowStartTime;
    uint64_t lastTime;

    std::vector<std::pair<uint64_t, double>> performanceMonitoring;
    std::map<std::string, double> monitoredQuantities;

    void updatePerformanceInfo(uint64_t time, double heartbeats);
    double computeWindowThroughput();

public:

    Monitor();

    void addPerformanceData(uint64_t stop, double heartbeats);
    double getValue(std::string parameter);
    unsigned int getWindowLength();
    uint64_t getStartTime();
    std::vector<double> getWindowPerformance();

};

#endif
