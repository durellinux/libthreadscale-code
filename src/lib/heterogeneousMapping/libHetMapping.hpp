#ifndef LIBHETMAPPING
#define LIBHETMAPPING

#include <map>
#include <vector>
#include <functional>

#include <monitor.hpp>
#include <inttypes.h>

#include <interpolationModel.hpp>
#include <utils.hpp>

typedef enum{NONE=0, CPU=1, GPU=2, FPGA=3} Resource;
typedef enum{UNBOUND=0, BOUND=1} KernelExecutionMode;

typedef struct{
    uint64_t start;
    uint64_t stop;
    std::string implementationName;
    double instantPerf;
    double windowPerf;
    double globalPerf;
} PerformanceReport;

typedef std::function<void(void)> Function;

typedef struct{
    Resource resource;
    std::string name;
    Function kernel;
    std::vector<double> performanceHistory;
    double expectedPerformance;
    double lastPerformance;
} Implementation;

class LibHetMapping{

private:

    uint64_t mappingRequests;

    int maxPerformanceHistoryPoints;
    int forgetAfterRequests;

    int implementationToUse;
    std::map<Resource, std::vector<double>> resourcePerformanceHistory;

    std::vector<Implementation> implementationsList;

    InterpolationModel model;

    double minQoS;
    double maxQoS;

    Monitor monitor;

    uint64_t lastRequest;
    int lastResponse;

    std::vector<PerformanceReport> performanceReport;

    std::string monitorGoal;

    void computeResource();
    void computeResourceToUse();

    double getAcceptablePerformance();

public:
    LibHetMapping();
    LibHetMapping(std::string performanceMeasure);

    ~LibHetMapping();

    int getResource();

    void setMinQoS(double qos, double slack);
    void sendHeartbeat(double heartbeats);

    void debugInfo();

    void dumpInfo(char *outputPrefix);
    std::vector<std::string> dumpPerformanceReport();

    void addImplementation(Resource resource, std::string name, Function kernel);
    void executeKernel(uint64_t heartbeats, KernelExecutionMode mode);

};

#endif
