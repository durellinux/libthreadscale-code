#include "libHetMapping.hpp"
#include <utils.hpp>
#include <iostream>
#include <fstream>
#include <unistd.h>

int LibHetMapping::getResource()
{
    mappingRequests++;
    computeResourceToUse();

    lastRequest = getTime();
    lastResponse = implementationToUse;

    PerformanceReport p;
    p.start = lastRequest;
    p.implementationName = implementationsList[lastResponse].name;

    performanceReport.push_back(p);

    return lastResponse;
}

void LibHetMapping::sendHeartbeat(double heartbeats)
{
    uint64_t start = getTime();

    uint64_t heartbeatTime = getTime();
    double time = heartbeatTime - lastRequest;

    int implementation = lastResponse;

    implementationsList[implementation].performanceHistory.push_back(heartbeats/time  * 1000 * 1000);

    for(auto &i : implementationsList){
        if(i.performanceHistory.size()>0 && (mappingRequests % forgetAfterRequests == 0))
            i.performanceHistory.erase(i.performanceHistory.begin());
    }

    monitor.addPerformanceData(heartbeatTime, heartbeats);

    auto p = performanceReport.end() - 1;
    p->stop = heartbeatTime;
    p->instantPerf = monitor.getValue(INSTANT_THROUGHPUT);
    p->windowPerf = monitor.getValue(WINDOW_THROUGHPUT);
    p->globalPerf = monitor.getValue(GLOBAL_THROUGHPUT);

    implementationsList[implementation].lastPerformance = monitor.getValue(INSTANT_THROUGHPUT);

    uint64_t end = getTime();
    addOverheadEvent(start, end, "SEND_HB");
}

double LibHetMapping::getAcceptablePerformance(){
    int N = 0;
    double currentValue = 0;
    double performanceFloor = minQoS;

    if(monitorGoal == WINDOW_THROUGHPUT){
        N = monitor.getWindowLength();
        std::vector<double> performanceVector = monitor.getWindowPerformance();

        double perfSum = 0;
        for(int i=1; i<performanceVector.size(); i++)
            perfSum += performanceVector[i];

        performanceFloor = minQoS * (N) - perfSum;

//        std::cout << "Goal: " << monitorGoal << " | Floor = " << minQoS << " * " << (N) << " - " << perfSum << " = " << performanceFloor << std::endl;
        performanceVector.push_back(performanceFloor);
//        dumpVector(performanceVector, 1, performanceVector.size());
    }

    if(monitorGoal == GLOBAL_THROUGHPUT){
        N = monitor.getValue(KERNEL_ITERATIONS);
        currentValue = monitor.getValue(TOTAL_HEARTBEATS);
        double expectedEndTime = (currentValue + monitor.getValue(AVERAGE_HEARTBEATS)) / minQoS * 1.0 ;
        double tNow = (getTime() * 1.0 - monitor.getStartTime()) / 1000000;
        if(expectedEndTime < tNow)
            expectedEndTime = tNow + 0.000001;
//        std::cout << "Expected " << expectedEndTime << " - Now: " << tNow << std::endl;
        double delta = (expectedEndTime - tNow) * 1.0;
        performanceFloor = monitor.getValue(AVERAGE_HEARTBEATS) / delta;

//        std::cout << "Goal: " << monitorGoal << " | Floor = " << monitor.getValue(AVERAGE_HEARTBEATS) << " / " << delta << " = " << performanceFloor << std::endl;
    }



    return performanceFloor;
}

void LibHetMapping::computeResource()
{
    uint64_t start = getTime();

//    double curPerf = monitor.getValue(monitorGoal);
//    double speedup = minQoS / curPerf;

    double acceptablePerformace = getAcceptablePerformance();

    int implementationId = -1;

    for(auto &i : implementationsList){
        if(i.performanceHistory.size() > 0)
            i.expectedPerformance = exponentialMovingAverage(i.performanceHistory, 0.98);
    }

    // Search in implementations with data
    for(int i=0; i<implementationsList.size(); i++){
        Implementation impl = implementationsList[i];
        if(impl.expectedPerformance > acceptablePerformace){
//            std::cout << "Picking right implementation" << std::endl;
            implementationId = i;
            break;
        }
    }

    // Search for implementation without profile info
    if(implementationId == -1){
        for(int i=0; i<implementationsList.size(); i++){
            Implementation impl = implementationsList[i];
            if(impl.lastPerformance < 0){
//                std::cout << "Picking first unknown" << std::endl;
                implementationId = i;
                break;
            }
        }
    }

    // Select one implementation (at random for now)
    if(implementationId == -1){
        int temp = 0;
        for(int i=0; i<implementationsList.size(); i++){
            if(implementationsList[i].lastPerformance > implementationsList[temp].lastPerformance){
                temp = 1;
            }
        }
//        std::cout << "Picking on the basis of last performance" << std::endl;
        implementationId = temp;
//        implementationId = rand() % implementationsList.size();
    }

    implementationToUse = implementationId;

//    std::cout << "Resource " << implementationsList[implementationToUse].name << std::endl;

    uint64_t stop = getTime();

    addOverheadEvent(start, stop, "UPDATE_MODEL");

}

void LibHetMapping::computeResourceToUse()
{
    uint64_t start = getTime();

    computeResource();

    uint64_t end = getTime();
    addOverheadEvent(start, end, "GET_THREADS");
}

void LibHetMapping::debugInfo()
{
//    std::cout << "Avg. performance: " << monitor.getValue(GLOBAL_THROUGHPUT) << " - " << lastThreadResponse <<  std::endl;
}

void LibHetMapping::dumpInfo(char *outputPrefix)
{
    std::vector<std::string> models = model.dumpModels();
    std::vector<std::string> overheads = dumpOverheads();
    std::vector<std::string> perfReport = dumpPerformanceReport();

    std::string s(outputPrefix);

    std::ofstream modelFile, overheadFile, perfReportFile;
    modelFile.open((s + "_Model.dat").c_str());
    overheadFile.open((s + "_Overhead.dat").c_str());
    perfReportFile.open((s + "_PerfReport.dat").c_str());

    for(auto &r : models)
        modelFile << r << std::endl;

    for(auto &r : overheads)
        overheadFile << r << std::endl;

    for(auto &r : perfReport)
        perfReportFile << r << std::endl;

    modelFile.close();
    overheadFile.close();
    perfReportFile.close();
}

std::vector<std::string>  LibHetMapping::dumpPerformanceReport()
{
    std::vector<std::string> output;
    for(auto &p : performanceReport){
        std::string s = "PERF_REPORT\t" + MAKE_STRING(p.start) + "\t" + MAKE_STRING(p.stop) + "\t" + MAKE_STRING(p.stop - p.start) + "\t" + MAKE_STRING(p.implementationName) + "\t" + MAKE_STRING(p.instantPerf) + "\t" + MAKE_STRING(p.windowPerf) + "\t" + MAKE_STRING(p.globalPerf);
        output.push_back(s);
    }
    return output;
}

LibHetMapping::LibHetMapping(std::string performanceMeasure)
{
    this->maxPerformanceHistoryPoints = 10;
    this->mappingRequests = 0;
    this->forgetAfterRequests = 5;

    this->lastResponse = CPU;
    this->implementationToUse = CPU;

    this->monitorGoal = performanceMeasure;
}

LibHetMapping::LibHetMapping(){
    this->maxPerformanceHistoryPoints = 10;
    this->mappingRequests = 0;
    this->forgetAfterRequests = 5;

    this->lastResponse = CPU;
    this->implementationToUse = CPU;

    this->monitorGoal = WINDOW_THROUGHPUT;
}

LibHetMapping::~LibHetMapping()
{

}

void LibHetMapping::setMinQoS(double qos, double slack)
{
    this->minQoS = qos;
    this->maxQoS = qos * (1 + slack);
}

void LibHetMapping::addImplementation(Resource resource, std::string name, Function kernel){
    Implementation i;
    i.kernel = kernel;
    i.resource = resource;
    i.name = name;
    i.lastPerformance = -1;

    implementationsList.push_back(i);
}

void LibHetMapping::executeKernel(uint64_t heartbeats, KernelExecutionMode mode){

    Function kernel = implementationsList[getResource()].kernel;

    uint64_t start = getTime();
    kernel();
    uint64_t end = getTime();

    uint64_t expected = heartbeats/maxQoS * 1000000; // Microseconds for one iteration
    uint64_t actual = end - start;

    if(mode==BOUND){
        if(expected>actual)
            usleep(expected-actual);
    }

    sendHeartbeat(heartbeats);
}
