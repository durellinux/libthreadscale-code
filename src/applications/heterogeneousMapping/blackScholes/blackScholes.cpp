#include <unistd.h>
#include <time.h>
#include <functional>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <iostream>
#include <libHetMapping.hpp>

#ifdef __APPLE__
    #include "OpenCL/opencl.h"
#else
    #include "CL/cl.h"
#endif

#include <sys/time.h>


#define OPT_N               6000000

#define MAX_SOURCE_SIZE (0x100000)

float RandFloat(float low, float high) {
    float t = (float)rand() / (float)RAND_MAX;
    return (1.0f - t) * low + t * high;
}


/************* OpenCL ****************************/

volatile int ready = 0;

typedef struct {
    cl_platform_id platform_id = NULL;
    cl_device_id device_id = NULL;
    cl_context context = NULL;
    cl_command_queue command_queue = NULL;
    cl_kernel kernel = NULL;
    cl_program program = NULL;
    cl_uint ret_num_devices;
    cl_uint ret_num_platforms;

    size_t source_size;
    char *source_str;
}OpenCLInfo;

OpenCLInfo openCLInfo;

void callback(cl_program p, void *user_data){
    ready = 1;
}

void initOpenCLGPU() {
    /* Open kernel file */
    FILE *fp;
    const char fileName[] = "./blackScholes.cl";
    cl_int ret;

    /* Load kernel source file */
    fp = fopen(fileName, "r");

    if (!fp) {
        std::cout << stderr << "Failed to load kernel." << std::endl;
        exit(1);
    }

    openCLInfo.source_str  = (char *)malloc(MAX_SOURCE_SIZE);

    openCLInfo.source_size = fread(openCLInfo.source_str, 1, MAX_SOURCE_SIZE, fp);

    //std::cout << openCLInfo.source_str << std::endl;

    fclose(fp);

    /* Get Platform/Device Information */
    ret = clGetPlatformIDs(1, &openCLInfo.platform_id, &openCLInfo.ret_num_platforms);

    /* Get GPU */
    ret = clGetDeviceIDs(openCLInfo.platform_id, CL_DEVICE_TYPE_GPU, 1, &openCLInfo.device_id, &openCLInfo.ret_num_devices);

    size_t max_global_work_size;
    ret = clGetDeviceInfo(openCLInfo.device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(max_global_work_size), &max_global_work_size, NULL);

    size_t max_work_item_size_dimension[3];
    ret = clGetDeviceInfo(openCLInfo.device_id, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(max_work_item_size_dimension), max_work_item_size_dimension, NULL);

    cl_uint max_compute_units;
    ret = clGetDeviceInfo(openCLInfo.device_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(max_compute_units), &max_compute_units, NULL);

    char buf[128];
    ret = clGetDeviceInfo(openCLInfo.device_id, CL_DEVICE_VERSION, 128, buf, NULL);

    /* Create context and queue */
    openCLInfo.context = clCreateContext(NULL, 1, &openCLInfo.device_id, NULL, NULL, &ret);
    openCLInfo.command_queue = clCreateCommandQueue(openCLInfo.context, openCLInfo.device_id, 0, &ret);

    /* Create kernel program from source file*/
    openCLInfo.program = clCreateProgramWithSource(openCLInfo.context, 1, (const char **)&openCLInfo.source_str, (const size_t *)&openCLInfo.source_size, &ret);
    ret = clBuildProgram(openCLInfo.program, 1, &openCLInfo.device_id, NULL, callback, NULL);

//    printf("%s\n", openCLInfo.source_str);

    if (ret == CL_BUILD_PROGRAM_FAILURE) {
        // Determine the size of the log
        size_t log_size;
        clGetProgramBuildInfo(openCLInfo.program, openCLInfo.device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);

        // Allocate memory for the log
        char *log = (char *) malloc(log_size);

        // Get the log
        clGetProgramBuildInfo(openCLInfo.program, openCLInfo.device_id, CL_PROGRAM_BUILD_LOG, log_size, log, NULL);

        // Print the log
        printf("%s\n", log);

        delete log;
    }

    while(!ready);

    //openCLInfo.kernel = clCreateKernel(openCLInfo.program, "square", &ret);
    openCLInfo.kernel = clCreateKernel(openCLInfo.program, "blackScholes", &ret);

    //CL_INVALID_MEM_OBJECT;

}

void destroyOpenCLGPU() {
    cl_int ret;

    ret = clFlush(openCLInfo.command_queue);
    ret = clFinish(openCLInfo.command_queue);
    ret = clReleaseKernel(openCLInfo.kernel);
    ret = clReleaseProgram(openCLInfo.program);
    ret = clReleaseCommandQueue(openCLInfo.command_queue);
    ret = clReleaseContext(openCLInfo.context);

    free(openCLInfo.source_str);
}
/************* End OpenCL ************************/


/************* CPU Kernel ******************+*****/


///////////////////////////////////////////////////////////////////////////////
// Polynomial approximation of cumulative normal distribution function
///////////////////////////////////////////////////////////////////////////////
static double CND(double d)
{
    const double       A1 = 0.31938153;
    const double       A2 = -0.356563782;
    const double       A3 = 1.781477937;
    const double       A4 = -1.821255978;
    const double       A5 = 1.330274429;
    const double RSQRT2PI = 0.39894228040143267793994605993438;

    double
    K = 1.0 / (1.0 + 0.2316419 * fabs(d));

    double
    cnd = RSQRT2PI * exp(- 0.5 * d * d) *
    (K * (A1 + K * (A2 + K * (A3 + K * (A4 + K * A5)))));

    if (d > 0)
        cnd = 1.0 - cnd;

    return cnd;
}


///////////////////////////////////////////////////////////////////////////////
// Black-Scholes formula for both call and put
///////////////////////////////////////////////////////////////////////////////
void blackScholesCPU(float &callResult, float &putResult, float stockPrice, float optionStrike, float optionYears, float risklessRate, float volatilityRate) {

    double S = stockPrice, X = optionStrike, T = optionYears, R = risklessRate, V = volatilityRate;

    double sqrtT = sqrt(T);
    double    d1 = (log(S / X) + (R + 0.5 * V * V) * T) / (V * sqrtT);
    double    d2 = d1 - V * sqrtT;
    double CNDD1 = CND(d1);
    double CNDD2 = CND(d2);

    //Calculate Call and Put simultaneously
    double expRT = exp(- R * T);
    callResult   = (float)(S * CNDD1 - X * expRT * CNDD2);
    putResult    = (float)(X * expRT * (1.0 - CNDD2) - S * (1.0 - CNDD1));
}

/************* End CPU Kernel ********************/


/************* GPU OpenCL Kernel *****************/

void blackScholes_OpenCL_GPU(float * callResult, float * putResult, float * stockPrice, float * optionStrike, float * optionYears, float risklessRate, float volatilityRate){

    cl_int ret;

    cl_mem mem_callResult = clCreateBuffer(openCLInfo.context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR ,sizeof(cl_float) * OPT_N, callResult, &ret);
    cl_mem mem_putResult = clCreateBuffer(openCLInfo.context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR ,sizeof(cl_float) * OPT_N, putResult, &ret);
    cl_mem mem_stockPrice = clCreateBuffer(openCLInfo.context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR ,sizeof(cl_float) * OPT_N, stockPrice, &ret);
    cl_mem mem_optionStrike = clCreateBuffer(openCLInfo.context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR ,sizeof(cl_float) * OPT_N, optionStrike, &ret);
    cl_mem mem_optionYears = clCreateBuffer(openCLInfo.context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR ,sizeof(cl_float) * OPT_N, optionYears, &ret);
    cl_mem mem_risklessRate = clCreateBuffer(openCLInfo.context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR ,sizeof(cl_float), &risklessRate, &ret);
    cl_mem mem_volatilityRate = clCreateBuffer(openCLInfo.context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR ,sizeof(cl_float), &volatilityRate, &ret);

    ret = clFinish(openCLInfo.command_queue);

    ret = clSetKernelArg(openCLInfo.kernel, 0, sizeof(cl_mem), &mem_callResult);
    ret = clSetKernelArg(openCLInfo.kernel, 1, sizeof(cl_mem), &mem_putResult);
    ret = clSetKernelArg(openCLInfo.kernel, 2, sizeof(cl_mem), &mem_stockPrice);
    ret = clSetKernelArg(openCLInfo.kernel, 3, sizeof(cl_mem), &mem_optionStrike);
    ret = clSetKernelArg(openCLInfo.kernel, 4, sizeof(cl_mem), &mem_optionYears);
    ret = clSetKernelArg(openCLInfo.kernel, 5, sizeof(cl_mem), &mem_risklessRate);
    ret = clSetKernelArg(openCLInfo.kernel, 6, sizeof(cl_mem), &mem_volatilityRate);

    /*Execute Kernels*/
    size_t global_work_size[3] = {OPT_N,1,1}; //set number of item per dimension

    ret = clEnqueueNDRangeKernel(openCLInfo.command_queue, openCLInfo.kernel, 1, NULL, global_work_size, NULL, 0, NULL, NULL);
    ret = clFinish(openCLInfo.command_queue);

    /* Copy output data from the output memory buffer */
    ret = clEnqueueReadBuffer(openCLInfo.command_queue, mem_callResult, CL_TRUE, 0, OPT_N*sizeof(cl_float), callResult, 0, NULL, NULL);
    ret = clEnqueueReadBuffer(openCLInfo.command_queue, mem_putResult, CL_TRUE, 0, OPT_N*sizeof(cl_float), putResult, 0, NULL, NULL);

    /* Destroy Buffer Object */
    ret = clReleaseMemObject(mem_callResult);
    ret = clReleaseMemObject(mem_putResult);
    ret = clReleaseMemObject(mem_stockPrice);
    ret = clReleaseMemObject(mem_optionStrike);
    ret = clReleaseMemObject(mem_optionYears);
    ret = clReleaseMemObject(mem_risklessRate);
    ret = clReleaseMemObject(mem_volatilityRate);
}

/************* End GPU OpenCL Kernel *************/


int main(int argc, char** argv) {
    int i;

    if(argc!=4){
        printf("USAGE: ./blackScholes ITERATIONS GOAL outputPrefix\n");
        exit(0);
    }

    unsigned long iterations = atoi(argv[1]);
    double goal = atof(argv[2]);
    char *outputPrefix = argv[3];

    float * h_CallResultCPU = (float *)malloc(OPT_N * sizeof(cl_float));
    float * h_PutResultCPU  = (float *)malloc(OPT_N * sizeof(cl_float));

    float * h_CallResultGPU = (float *)malloc(OPT_N * sizeof(cl_float));
    float * h_PutResultGPU  = (float *)malloc(OPT_N * sizeof(cl_float));

    float * h_StockPrice    = (float *)malloc(OPT_N * sizeof(cl_float));
    float * h_OptionStrike  = (float *)malloc(OPT_N * sizeof(cl_float));
    float * h_OptionYears   = (float *)malloc(OPT_N * sizeof(cl_float));

    const float riskFree = 0.02f;
    const float volatility = 0.30f;

    //**** GPU
    initOpenCLGPU();
    //********

    /*Create Random data*/

    srand48(time(NULL));

    //Generate options set
    for (i = 0; i < OPT_N; i++) {

        h_CallResultCPU[i] = 0.0f;
        h_PutResultCPU[i]  = -1.0f;

        h_CallResultGPU[i] = 0.0f;
        h_PutResultGPU[i]  = -1.0f;

        h_StockPrice[i]    = RandFloat(5.0f, 30.0f);
        h_OptionStrike[i]  = RandFloat(1.0f, 100.0f);
        h_OptionYears[i]   = RandFloat(0.25f, 10.0f);
    }

    /******************/

    // Instrumentation

    LibHetMapping libHet;
    libHet.setMinQoS(goal, 0.10);

    /*CPU Implementation*/
    Function cpuImpl = [&] () -> void {
        for (int j = 0; j < OPT_N; ++j) {
            blackScholesCPU(h_CallResultCPU[i], h_PutResultCPU[i], h_StockPrice[i], h_OptionStrike[i], h_OptionYears[i], riskFree, volatility);
        }
    };

    Function ompImpl = [&] () -> void {
        #pragma omp parallel for
        for (int j = 0; j < OPT_N; ++j) {
            blackScholesCPU(h_CallResultCPU[i], h_PutResultCPU[i], h_StockPrice[i], h_OptionStrike[i], h_OptionYears[i], riskFree, volatility);
        }
    };


    /*GPU Implementation*/
    Function gpuImpl = [&] () -> void {
        blackScholes_OpenCL_GPU(h_CallResultCPU, h_PutResultCPU, h_StockPrice, h_OptionStrike, h_OptionYears, riskFree, volatility);
    };

    libHet.addImplementation(CPU, "Serial", cpuImpl);
//    libHet.addImplementation(CPU, "OpenMP", ompImpl);
    libHet.addImplementation(GPU, "GPU", gpuImpl);

    /****************************/

    // Executing

    for (i = 0; i < iterations; ++i) {
        std::cout << i << std::endl;
        libHet.executeKernel(OPT_N, UNBOUND);
    }

    libHet.dumpInfo(outputPrefix);

    /* Finalization */
    destroyOpenCLGPU();
    /****************/

    free(h_OptionYears);
    free(h_OptionStrike);
    free(h_StockPrice);
    free(h_PutResultCPU);
    free(h_CallResultCPU);

    return 0;

}
