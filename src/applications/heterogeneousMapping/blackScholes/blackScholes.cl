
/*
 * For a description of the algorithm and the terms used, please see the
 * documentation for this sample.
 *
 * On invocation of kernel blackScholes, each work thread calculates call price
 * and put price values for given stoke price, option strike price,
 * time to expiration date, risk free interest and volatility factor.
 */

#define S_LOWER_LIMIT 10.0f
#define S_UPPER_LIMIT 100.0f
#define K_LOWER_LIMIT 10.0f
#define K_UPPER_LIMIT 100.0f
#define T_LOWER_LIMIT 1.0f
#define T_UPPER_LIMIT 10.0f
#define R_LOWER_LIMIT 0.01f
#define R_UPPER_LIMIT 0.05f
#define SIGMA_LOWER_LIMIT 0.01f
#define SIGMA_UPPER_LIMIT 0.10f

#define float_type float

/**
 * @brief   Abromowitz Stegun approxmimation for PHI (Cumulative Normal Distribution Function)
 * @param   X input value
 * @param   phi pointer to store calculated CND of X
 */
inline void phi(float_type X, float_type* phi)
{
    float_type y;
    float_type absX;
    float_type t;
    float_type result;
    
    const float_type c1 = (float_type)0.319381530f;
    const float_type c2 = (float_type)-0.356563782f;
    const float_type c3 = (float_type)1.781477937f;
    const float_type c4 = (float_type)-1.821255978f;
    const float_type c5 = (float_type)1.330274429f;
    
    const float_type zero = (float_type)0.0f;
    const float_type one = (float_type)1.0f;
    const float_type two = (float_type)2.0f;
    const float_type temp4 = (float_type)0.2316419f;
    
    const float_type oneBySqrt2pi = (float_type)0.398942280f;
    
    absX = fabs(X);
    t = one/(one + temp4 * absX);
    
    y = one - oneBySqrt2pi * exp(-X*X/two) * t
    * (c1 + t
       * (c2 + t
          * (c3 + t
             * (c4 + t * c5))));
    
    result = (X < zero)? (one - y) : y;
    
    *phi = result;
}


kernel void blackScholes(global float * callResult, global float * putResult, global float * stockPrice, global float * optionStrike, global float * optionYears,global float * risklessRate, global float * volatilityRate) {
    
    unsigned int xPos = get_global_id(0);
    
    float_type S = stockPrice[xPos];
    float_type X = optionStrike[xPos];
    float_type T = optionYears[xPos];
    float_type R = *risklessRate;
    float_type V = *volatilityRate;
    
    float_type sqrtT = sqrt(T);
    float_type    d1 = (log(S / X) + (R + 0.5 * V * V) * T) / (V * sqrtT);
    float_type    d2 = d1 - V * sqrtT;
    float_type CNDD1;
    float_type CNDD2;
    
    phi(d1, &CNDD1);
    phi(d2, &CNDD2);
    
    float_type expRT = exp(- R * T);
    callResult[xPos] = (float_type)(S * CNDD1 - X * expRT * CNDD2);
    putResult[xPos] = (float_type)(X * expRT * (1.0 - CNDD2) - S * (1.0 - CNDD1));
}