//
//  main.cpp
//  SampleApp
//
//  Created by Raegal on 16/03/15.
//  Copyright (c) 2015 ingconti.com. All rights reserved.
//

#include <unistd.h>
#include <time.h>
#include <functional>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <getopt.h>
#include <iostream>
#include <string.h>

#include <libHetMapping.hpp>

#ifdef __APPLE__
#include "OpenCL/opencl.h"
#else
#include <omp.h>
#include <bsd/stdio.h>
#include <bsd/stdlib.h>
#include "CL/cl.h"
#endif

#include <sys/time.h>

#define NUM_OF_COLUMNS          50
#define NUM_OF_ROWS             10000 //22000
#define NUM_OF_CHUNKS_PER_DIM   5 //22000


#define MAX_SOURCE_SIZE (0x100000)

/***************** Options ***********************/

typedef struct globalArgs_t{
    unsigned int numberOfIterations;
    unsigned int numberOfPixels;
    unsigned int numberOfSuccFrame;
    unsigned int numberOfHorizontalChunk;
    unsigned int numberOfVerticalChunk;
    double qos;
    char outputPrefix[50];
    std::string optimizationGoal;
} GlobalArgs;

static const char *optString = "n:p:k:h:v:q:o:g:";

static struct option longOpts[] = {
    { "numberOfIterations", required_argument, NULL, 'n' },
    { "numberOfPixels", required_argument, NULL, 'p' },
    { "numberOfSuccFrame", required_argument, NULL, 'k' },
    { "numberOfHorizontalCut", required_argument, NULL, 'h' },
    { "numberOfVerticalCut", required_argument, NULL, 'v' },
    { "QoS", required_argument, NULL, 'q' },
    { "outputPrefix", required_argument, NULL, 'o' },
    { "optimizationGoal", required_argument, NULL, 'g' }
};


GlobalArgs globalArgs;


void parseCommmand(int argc, char** argv){
    int longIndex;
    int opt;
    
    while((opt = getopt_long(argc, argv, optString, longOpts, &longIndex)) != -1 ) {
        printf("%c = %s\n", opt, optarg);
        switch( opt ) {
                
            case 'n':
                globalArgs.numberOfIterations = std::atoi(optarg);
                break;
                
            case 'p':
                globalArgs.numberOfPixels = std::atoi(optarg);
                break;
                
            case 'k':
                globalArgs.numberOfSuccFrame = std::atoi(optarg);
                break;
                
            case 'h':
                globalArgs.numberOfHorizontalChunk = std::atoi(optarg);
                break;
                
            case 'v':
                globalArgs.numberOfVerticalChunk = std::atoi(optarg);
                break;

            case 'q':
                globalArgs.qos = std::atof(optarg);
                break;

            case 'o':
                strcpy(globalArgs.outputPrefix, optarg);
                printf("Parsing o: %s", optarg);
                break;

            case 'g':
                if(!strcmp(optarg, "deadline")){
                    globalArgs.optimizationGoal = GLOBAL_THROUGHPUT;
                }
                else
                    globalArgs.optimizationGoal = WINDOW_THROUGHPUT;
                break;

            default:
                break;
        }
    }
    
    
}

/***************** End Options ********************/


/************* OpenCL ****************************/

volatile int ready = 0;

typedef struct {
    cl_platform_id platform_id = NULL;
    cl_device_id device_id = NULL;
    cl_context context = NULL;
    cl_command_queue command_queue = NULL;
    cl_kernel kernel = NULL;
    cl_program program = NULL;
    cl_uint ret_num_devices;
    cl_uint ret_num_platforms;
    
    size_t source_size;
    char *source_str;
}OpenCLInfo;

OpenCLInfo openCLInfo;

void callback(cl_program p, void *user_data){
    ready = 1;
}

void initOpenCLGPU() {
    /* Open kernel file */
    FILE *fp;
    const char fileName[] = "./pearsonCorrelation.cl";
    cl_int ret;
    
    /* Load kernel source file */
    fp = fopen(fileName, "r");
    
    if (!fp) {
        std::cout << stderr << " Failed to load kernel." << std::endl;
        exit(1);
    }
    
    openCLInfo.source_str  = (char *)malloc(MAX_SOURCE_SIZE);
    
    openCLInfo.source_size = fread(openCLInfo.source_str, 1, MAX_SOURCE_SIZE, fp);
    
    //std::cout << openCLInfo.source_str << std::endl;
    
    
    fclose(fp);
    
    /* Get Platform/Device Information */
    ret = clGetPlatformIDs(1, &openCLInfo.platform_id, &openCLInfo.ret_num_platforms);
    
    /* Get GPU */
    ret = clGetDeviceIDs(openCLInfo.platform_id, CL_DEVICE_TYPE_GPU, 1, &openCLInfo.device_id, &openCLInfo.ret_num_devices);
    
    size_t max_global_work_size;
    clGetDeviceInfo(openCLInfo.device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(max_global_work_size), &max_global_work_size, NULL);
    
    size_t max_work_item_size_dimension[3];
    clGetDeviceInfo(openCLInfo.device_id, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(max_work_item_size_dimension), max_work_item_size_dimension, NULL);
    
    cl_uint max_compute_units;
    clGetDeviceInfo(openCLInfo.device_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(max_compute_units), &max_compute_units, NULL);
    
    char buf[128];
    clGetDeviceInfo(openCLInfo.device_id, CL_DEVICE_VERSION, 128, buf, NULL);
    
    
    /* Create context and queue */
    openCLInfo.context = clCreateContext(NULL, 1, &openCLInfo.device_id, NULL, NULL, &ret);
    openCLInfo.command_queue = clCreateCommandQueue(openCLInfo.context, openCLInfo.device_id, 0, &ret);
    
    /* Create kernel program from source file*/
    openCLInfo.program = clCreateProgramWithSource(openCLInfo.context, 1, (const char **)&openCLInfo.source_str, (const size_t *)&openCLInfo.source_size, &ret);
    ret = clBuildProgram(openCLInfo.program, 1, &openCLInfo.device_id, NULL, callback, NULL);
    
    if (ret == CL_BUILD_PROGRAM_FAILURE) {
        // Determine the size of the log
        size_t log_size;
        clGetProgramBuildInfo(openCLInfo.program, openCLInfo.device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
        
        // Allocate memory for the log
        char *log = (char *) malloc(log_size);
        
        // Get the log
        clGetProgramBuildInfo(openCLInfo.program, openCLInfo.device_id, CL_PROGRAM_BUILD_LOG, log_size, log, NULL);
        
        // Print the log
        printf("%s\n", log);
        
        delete log;
    }
    
    while(!ready);
    
    openCLInfo.kernel = clCreateKernel(openCLInfo.program, "Correlation_2D", &ret);
    
    //CL_INVALID_MEM_OBJECT;
    
}

void destroyOpenCLGPU() {
    cl_int ret;
    
    ret = clFlush(openCLInfo.command_queue);
    ret = clFinish(openCLInfo.command_queue);
    ret = clReleaseKernel(openCLInfo.kernel);
    ret = clReleaseProgram(openCLInfo.program);
    ret = clReleaseCommandQueue(openCLInfo.command_queue);
    ret = clReleaseContext(openCLInfo.context);
    
    free(openCLInfo.source_str);
}
/************* End OpenCL ************************/


/************* CPU Kernel ******************+*****/

void Correlation_CPU_2D(float * inputMatrix, float * inputStdDev, float * outputMatrix, unsigned int nThread){
    
    #pragma omp parallel for num_threads(nThread)

    for (int row = 0; row < globalArgs.numberOfPixels; ++row) {
        for (int col = 0; col < globalArgs.numberOfPixels; ++col) {
            
            if (col > row)
                break;
            
            if (col == row) {
                outputMatrix[row*globalArgs.numberOfPixels + col] = 1.0;
                break;
            }
            
            float temp = 0;
            
            float tot_dev = inputStdDev[row] * inputStdDev[col] * globalArgs.numberOfSuccFrame;
            
            for (int k = 0; k < globalArgs.numberOfSuccFrame; ++k) {
                temp = inputMatrix[row*globalArgs.numberOfSuccFrame + k] * inputMatrix[col*globalArgs.numberOfSuccFrame + k];
            }
            
            temp /= tot_dev;
            
            if (row == col)
                temp = 1.0;
            
            outputMatrix[row*globalArgs.numberOfPixels + col] = temp;
        }
    }
}

/************* End CPU Kernel ********************/


/************* GPU OpenCL Kernel *****************/

void Correlation_OpenCL_GPU_2D(float * inputMatrix, float * inputStdDev, float * outputMatrix){
    
    cl_int ret;
    
    cl_mem mem_inputMatrix = clCreateBuffer(openCLInfo.context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR ,sizeof(cl_float) * globalArgs.numberOfPixels * globalArgs.numberOfSuccFrame, inputMatrix, &ret);
    cl_mem mem_inputStdDev = clCreateBuffer(openCLInfo.context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR ,sizeof(cl_float) * globalArgs.numberOfPixels, inputStdDev, &ret);
    cl_mem mem_outputMatrix = clCreateBuffer(openCLInfo.context, CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR ,sizeof(cl_float) * globalArgs.numberOfPixels/globalArgs.numberOfHorizontalChunk * globalArgs.numberOfPixels/globalArgs.numberOfVerticalChunk, outputMatrix, &ret);
    
    ret = clFinish(openCLInfo.command_queue);
    
    ret = clSetKernelArg(openCLInfo.kernel, 0, sizeof(cl_mem), &mem_inputMatrix);
    ret = clSetKernelArg(openCLInfo.kernel, 1, sizeof(cl_mem), &mem_inputStdDev);
    ret = clSetKernelArg(openCLInfo.kernel, 2, sizeof(cl_mem), &mem_outputMatrix);
    
    /*Execute Kernels*/
    size_t global_work_size[3] = {globalArgs.numberOfPixels/globalArgs.numberOfHorizontalChunk, globalArgs.numberOfPixels/globalArgs.numberOfVerticalChunk, 0}; //set number of item per dimension
    size_t region[3] = {globalArgs.numberOfPixels/globalArgs.numberOfVerticalChunk*sizeof(float), globalArgs.numberOfPixels/globalArgs.numberOfHorizontalChunk, 1};
    size_t buffer_origin[3] = {0,0,0};
    
    int * offset = (int*)malloc(2*sizeof(int));
    size_t host_origin[3] = {0,0,0};
    
    cl_mem mem_offset = clCreateBuffer(openCLInfo.context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR ,sizeof(int)*2, offset, &ret);
    
    for (int i = 0; i < globalArgs.numberOfVerticalChunk; ++i) {
        for (int j = 0; j < globalArgs.numberOfHorizontalChunk; ++j) {
            
            offset[0] = i*globalArgs.numberOfPixels/globalArgs.numberOfVerticalChunk; // in kernel the [0] is used for col
            offset[1] = j*globalArgs.numberOfPixels/globalArgs.numberOfHorizontalChunk; // in kernel in [1] is used for row
            
//            std::cout << "offset[0] " << offset[0] << " offset[1] " << offset[1] << std::endl;
            
            ret = clEnqueueWriteBuffer(openCLInfo.command_queue, mem_offset, CL_TRUE, 0, 2*sizeof(cl_int), offset, 0, NULL, NULL);
            
            ret = clSetKernelArg(openCLInfo.kernel, 3, sizeof(cl_mem), &mem_offset);
            
            ret = clEnqueueNDRangeKernel(openCLInfo.command_queue, openCLInfo.kernel, 2, NULL, global_work_size, NULL, 0, NULL, NULL);
            
            ret = clFinish(openCLInfo.command_queue);
            
            host_origin[0] = (size_t)offset[0]*sizeof(float);
            host_origin[1] = (size_t)offset[1];
            
            //std::cout << "offset: " << host_origin[0]+host_origin[1]*globalArgs.numberOfPixels*sizeof(float) << std::endl;
            
            ret = clEnqueueReadBufferRect(openCLInfo.command_queue, mem_outputMatrix, CL_TRUE, buffer_origin, host_origin, region, 0, 0, globalArgs.numberOfPixels*sizeof(float), 0, outputMatrix, 0, NULL, NULL);
            
        }
    }
    
    /* Destroy Buffer Object */
    
    delete offset;
    
    ret = clReleaseMemObject(mem_inputMatrix);
    ret = clReleaseMemObject(mem_inputStdDev);
    ret = clReleaseMemObject(mem_outputMatrix);
    ret = clReleaseMemObject(mem_offset);
}

/************* End GPU OpenCL Kernel *************/


int main(int argc, char** argv){
    
    parseCommmand(argc, argv);
    
    int i;
    
    float * corrInput = (float*)malloc(sizeof(cl_float) * globalArgs.numberOfPixels * globalArgs.numberOfSuccFrame); //aligned
    float * corrOutput = (float*)malloc(sizeof(cl_float) * globalArgs.numberOfPixels * globalArgs.numberOfPixels); //aligned
    float * corrOutputGPU = (float*)malloc(sizeof(cl_float) * globalArgs.numberOfPixels * globalArgs.numberOfPixels); //aligned
    
    float * corrInputStdDev = (float*)malloc(sizeof(cl_float) * globalArgs.numberOfPixels); //aligned
    
    //**** GPU
    initOpenCLGPU();
    //********
    
    /*Create Fake data*/
    srand48(time(NULL));
    
    for (int i = 0; i < globalArgs.numberOfPixels * globalArgs.numberOfSuccFrame; ++i) {
        corrInput[i] = drand48();
    }
    
    for (int i = 0; i < globalArgs.numberOfPixels; ++i) {
        corrInputStdDev[i] = 1.0;
    }
    
    /******************/

    // Instrumentation

    LibHetMapping libHet(globalArgs.optimizationGoal);
    libHet.setMinQoS(globalArgs.qos, 0.10);

    /*CPU Implementation*/
    Function cpuImpl = [&] () -> void {
        std::cout << "CPU" << std::endl;
        unsigned int nThread = 1; // TODO Set threads?
        Correlation_CPU_2D(corrInput,corrInputStdDev,corrOutput,nThread);
    };
    
    /*GPU Implementation*/
    Function gpuImpl = [&] () -> void {
        std::cout << "GPU" << std::endl;
        Correlation_OpenCL_GPU_2D(corrInput,corrInputStdDev,corrOutputGPU);
    };

    libHet.addImplementation(CPU, "CPU", cpuImpl);
    libHet.addImplementation(GPU, "GPU", gpuImpl);
    

    for (i = 0; i<globalArgs.numberOfIterations; i++) {
        std::cout << i << std::endl;
        libHet.executeKernel(1, UNBOUND);
    }

    printf("Writing to: %s\n", globalArgs.outputPrefix);

    libHet.dumpInfo(globalArgs.outputPrefix);
    
    /* Finalization */
    destroyOpenCLGPU();
    /****************/
    
    free(corrInput);
    free(corrInputStdDev);
    free(corrOutput);
    free(corrOutputGPU);
    
    return 0;
    
}
