#include <iostream>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include <libHetMapping.hpp>

void CPUkernel(uint64_t time){
    usleep(time);
}

void GPUkernel(uint64_t time){
    usleep(time);
}

int main(int argc, char **argv){

    if(argc!=4){
        std::cout << "USAGE: testApp ITERATIONS GOAL OUTPUT_PREFIX" << std::endl;
        exit(0);
    }

    int iterations = atoi(argv[1]);
    double goal = atof(argv[2]);
    char *outputPrefix = argv[3];
    int i;

    LibHetMapping libHet;
    libHet.setMinQoS(goal, 0.10);

    uint64_t CPUtime = 100000;
    uint64_t GPUtime = 30000;

    Function cpuFunction0 = [&] () {
        CPUkernel(CPUtime);
    };

    Function cpuFunction1 = [&] () {
        CPUkernel(CPUtime/2);
    };

    Function gpuFunction0 = [&] () {
        GPUkernel(GPUtime);
    };

    libHet.addImplementation(CPU, "CPU_0", cpuFunction0);
    libHet.addImplementation(CPU, "CPU_1", cpuFunction1);
    libHet.addImplementation(GPU, "GPU_0", gpuFunction0);

    std::cout << "Before" << std::endl;

    for(i=0; i<iterations; i++){
        std::cout << i << std::endl;
        libHet.executeKernel(1, UNBOUND);
    }

    std::cout << "Done" << std::endl;

    libHet.dumpInfo(outputPrefix);
}
