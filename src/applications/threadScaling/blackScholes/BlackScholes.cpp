#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <omp.h>

#include "BlackScholes.h"

#include <libThreadScale.hpp>

float RandFloat(float low, float high)
{
    float t = (float)rand() / (float)RAND_MAX;
    return (1.0f - t) * low + t * high;
}

////////////////////////////////////////////////////////////////////////////////
// Data configuration
////////////////////////////////////////////////////////////////////////////////
const int OPT_N = 400000;
int  NUM_ITERATIONS = 50 ;


const int          OPT_SZ = OPT_N * sizeof(float);
const float      RISKFREE = 0.02f;
const float    VOLATILITY = 0.30f;

#define DIV_UP(a, b) ( ((a) + (b) - 1) / (b) )

#define __getTime(tv, t) gettimeofday(&tv, NULL); t = tv.tv_sec * 1000*1000 + tv.tv_usec;
#define __curDevice(device, i) i>0 ? device[i-1] : 0;

//Results calculated by CPU for reference
//CPU copy of GPU results
//CPU instance of input data
float *h_PutResultCPU;
float *h_CallResultCPU;
float *h_StockPrice, *h_OptionStrike, *h_OptionYears;

unsigned int kernel(unsigned int threads){
#pragma omp parallel for
    for (int opt = 0; opt < OPT_N; opt++){
        BlackScholesBodyCPU(
            h_CallResultCPU[opt],
            h_PutResultCPU[opt],
            h_StockPrice[opt],
            h_OptionStrike[opt],
            h_OptionYears[opt],
            RISKFREE,
            VOLATILITY
        );
    }

    return threads;
}

int main(int argc, char **argv)
{
    // Start logs
    // printf("[%s] - Starting...\n", argv[0]);

    int i;

    if(argc!=5){
        printf("USAGE: BlackScholes ITERATIONS GOAL[data/s] OUTPUT_PREFIX FORCE_THREADS\n");
        exit(0);
    }

    NUM_ITERATIONS = atoi(argv[1]);
    float goal = atof(argv[2]);
    char *outputPrefix = argv[3];
    unsigned int forceThreads = atoi(argv[4]);

    // printf("Initializing data...\n");
    // printf("...allocating CPU memory for options.\n");
    h_CallResultCPU = (float *)malloc(OPT_SZ);
    h_PutResultCPU  = (float *)malloc(OPT_SZ);
    h_StockPrice    = (float *)malloc(OPT_SZ);
    h_OptionStrike  = (float *)malloc(OPT_SZ);
    h_OptionYears   = (float *)malloc(OPT_SZ);

    // printf("...generating input data in CPU mem.\n");
    srand(5347);

        //Generate options set
    for (i = 0; i < OPT_N; i++)
    {
        h_CallResultCPU[i] = 0.0f;
        h_PutResultCPU[i]  = -1.0f;
        h_StockPrice[i]    = RandFloat(5.0f, 30.0f);
        h_OptionStrike[i]  = RandFloat(1.0f, 100.0f);
        h_OptionYears[i]   = RandFloat(0.25f, 10.0f);
    }

    LibThreadScale libQoS;
    libQoS.setMinQoS(goal);
    libQoS.setForceThreads(forceThreads);

    for(i = 0; i<NUM_ITERATIONS; i++){
        kernel(forceThreads);
        libQoS.sendHeartbeat(OPT_N);
        // printf("Cur iterations: %d\t%d\t%f\t%f\t%d\n", opts[i], times[i], avg, target, threads[i]);
    }

    libQoS.dumpInfo(outputPrefix);

    // printf("...releasing CPU memory.\n");
    free(h_OptionYears);
    free(h_OptionStrike);
    free(h_StockPrice);
    free(h_PutResultCPU);
    free(h_CallResultCPU);
    // printf("Shutdown done.\n");

    // printf("Test passed\n");
    exit(EXIT_SUCCESS);
}
