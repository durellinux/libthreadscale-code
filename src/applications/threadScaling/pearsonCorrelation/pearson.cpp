#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <math.h>
#include <sys/time.h>

#include <omp.h>
#include <iostream>

#include <libThreadScale.hpp>

int P = 4;
int K = 4;
#define float_type float

unsigned int ITERATIONS = 1;
float GOAL = 1.0;

void initValues(float_type * mat, int rows, int cols){
    int i, j;
    for(i = 0; i<rows; i++)
        for(j=0; j<cols; j++)
            // mat[i*K + j] = 5;
            // mat[i*K + j] = rand() % 255;
            mat[i*K + j] = i + j;
}

void outputValues(float_type *mat, int vals){
    int i, j;
    for(i = 0; i<vals; i++){
        for(j=0; j<vals; j++)
            printf("%f\t", mat[i*vals + j]);

        printf("\n");
    }
}

unsigned int pearsonCorrelation_OpenMP(float_type *pixel, float_type *dep, float_type *devs, float_type *corr, int p, int k, unsigned int threads){
    int c1,c2;

#pragma omp parallel for num_threads(threads) private(c2) firstprivate(corr, devs, dep)
    for(c1=0; c1<p; c1++){
        for(c2=0; c2<p; c2++){
//            printf("JOB %d\t%d\t%d\n",omp_get_thread_num(), c1, c2);
            if(c1==c2){
                corr[c1*p + c2] = 1;
            }  
            if(c1>c2){
                float_type tmp = 0;
                // *(corr + c1*P + c2)=0;
                int s;
                for(s=0; s<k; s++){
                    // *(corr + c1*P + c2)=*(corr + c1*P + c2)+dep[c1 * K + s]*dep[c2 * K + s];
                    tmp = tmp + dep[c1 * k + s]*dep[c2 * k + s];
                }
                float_type devtot;
                devtot=devs[c1]*devs[c2]*k;
                corr[c1*p + c2] = tmp / devtot;
                corr[c2*p + c1] = corr[c1*p + c2];
            }  
        }
    }

    return threads;
}

int main(int argc, char **argv){


    if(argc!=6){
        printf("USAGE: correlation P K ITERATIONS GOALS OUTPUT_PREFIX \n");
        exit(1);
    }

    P = atoi(argv[1]);
    K = atoi(argv[2]);
    ITERATIONS = atoi(argv[3]);
    GOAL = atof(argv[4]);
    char *outputPrefix = argv[5];

    float_type *media = (float_type *) malloc(sizeof(float_type) * P);
    float_type *pixel = (float_type *) malloc(sizeof(float_type) * P * K);
    float_type *dep = (float_type *) malloc(sizeof(float_type) * P * K);
    float_type *devs = (float_type *) malloc(sizeof(float_type) * P);
    // float_type *corr_serial = (float_type *) malloc(sizeof(float_type) * P * P);
    float_type *corr = (float_type *) malloc(sizeof(float_type) * P * P);

    int i,j,k,r,a,b;
    unsigned int iter;

    assert(pixel != NULL);
    assert(dep != NULL);
    assert(corr != NULL);
    assert(devs != NULL);
    assert(media != NULL);

    initValues(pixel, P, K);
    // outputValues(pixel, P);

    LibThreadScale libQoS;
    libQoS.setMinQoS(GOAL);

    for(iter=0; iter<ITERATIONS; iter++){

        int threads = libQoS.getThreadsToUse();

        // MEAN
        for (i=0; i<P; i++){
            media[i]=0;
            // printf("Computing avg for ROW: %d\n", i);
            for(j=0; j<K; j++){
                media[i]=media[i] + *(pixel + K*i + j);
            }
            media[i]=media[i]/K;
        }

        // DEPOLARIZATION
        for(k=0; k<P; k++){
            for(r=0; r<K; r++){
                dep[k * K + r]=*(pixel + k*K + r) - media[k];
            }
        }

        // STANDARD DEVIATION
        for(a=0; a<P; a++){
            devs[a]=0;
            for(b=0; b<K; b++){
                devs[a]=devs[a]+dep[a * K + b]*dep[a * K + b];
            }
            devs[a]=devs[a]/(K-1.0);
            devs[a]=sqrt(devs[a]);
        }

        // CORRELATION
        pearsonCorrelation_OpenMP(pixel, dep, devs, corr, P, K, threads);

        libQoS.sendHeartbeat(K);

    }

    free(media);
    free(devs);
    free(pixel);
    free(dep);
    free(corr);

    libQoS.dumpInfo(outputPrefix);


    return 0;
}
