#include <iostream>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include <libThreadScale.hpp>

void kernel(int threads){
    uint64_t kernelLatency = 1000000;
    usleep(kernelLatency/threads);
}

int main(int argc, char **argv){

    if(argc!=3){
        std::cout << "USAGE: testApp ITERATIONS OUTPUT_PREFIX" << std::endl;
        exit(0);
    }

    int iterations = atoi(argv[1]);
    char *outputPrefix = argv[2];
    int i;

    LibThreadScale libQoS;
    libQoS.setMinQoS(10);

    for(i=0; i<iterations; i++){
        kernel(libQoS.getThreadsToUse());
        libQoS.sendHeartbeat(1);
//        kernel(1);
    }

//    dumpOverheads();
    libQoS.dumpInfo(outputPrefix);
}
