//
//  main.cpp
//  SampleApp
//
//  Created by Raegal on 16/03/15.
//  Copyright (c) 2015 ingconti.com. All rights reserved.
//

#include <unistd.h>
#include <time.h>
#include <functional>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <getopt.h>
#include <sys/time.h>
#include <omp.h>

#include "AppPerformanceMonitor.h"

#include "ImageProcessingHeaders.h"

#include "ReportMonitor.h"

#define TWIST_FACTOR 0.9
#define GAUSS_SIGMA 2.5

typedef enum{OPENCL_CPU = 0, OPENMP} CPU_Implementation;



typedef struct globalArgs_t{
    std::string imageNamePattern;
    std::string outputNamePattern;
    std::string pathToImages;
    unsigned int startNum;
    unsigned int endNum;
    CPU_Implementation target;
} GlobalArgs;

static const char *optString = "i:o:s:e:t:p:";

static struct option longOpts[] = {
    { "imageNamePattern", required_argument, NULL, 'i' },
    { "outputNamePattern", required_argument, NULL, 'o' },
    { "startNum", required_argument, NULL, 's' },
    { "endNum", required_argument, NULL, 'e' },
    { "target", required_argument, NULL, 't' },
    { "pathToImages", optional_argument, NULL, 'p' },
};

GlobalArgs globalArgs;

CPU_Implementation cpuImplementationSelected(std::string type)
{
    if (type.compare("OPENCL_CPU") == 0)
        return OPENCL_CPU;
    
    return OPENMP;
}


void parseCommmand(int argc, char** argv){
    int longIndex;
    int opt;
    
    while((opt = getopt_long_only(argc, argv, optString, longOpts, &longIndex)) != -1 ) {
        switch( opt ) {
                
            case 'i':
                globalArgs.imageNamePattern = optarg;
                break;
                
            case 'o':
                globalArgs.outputNamePattern = optarg;
                break;
                
            case 's':
                globalArgs.startNum = std::atoi(optarg);
                break;
                
            case 'e':
                globalArgs.endNum = std::atoi(optarg);
                break;
                
            case 't':
                globalArgs.target = cpuImplementationSelected(optarg);
                break;
                
            case 'p':
                globalArgs.pathToImages = optarg;
                break;
        }
    }
    
    
}

/***************************************************/
/************* Open MP Implementation **************/
/***************************************************/

void openMPImplementation(GlobalArgs globalArgs, unsigned int nThread){
    sImage originalImage, twistedImage, twistedImage1, filteredImage, greyImage, outputImage;
    int rows = 0;
    int cols = 0;
    int depth = 0;
    int img;
    
    std::string fileName;
    
    for(img = globalArgs.startNum; img <= globalArgs.endNum; img++){
        
        if (globalArgs.pathToImages.length() == 0)
            fileName = MAKE_STRING(globalArgs.imageNamePattern << img << ".bmp");
        else
            fileName = MAKE_STRING(globalArgs.pathToImages << "/" << globalArgs.imageNamePattern << img << ".bmp");
        
        std::cout << "Processing " << fileName << std::endl;
        
        readImage(fileName.c_str(), &originalImage);
        
        //buffers are reinstantiated only when the image size changes
        if(rows!=originalImage.rows || cols!=originalImage.cols || depth!= originalImage.depth){
            
            if(img!=globalArgs.startNum){
                //Delete
                deleteImage(&twistedImage);
                deleteImage(&twistedImage1);
                deleteImage(&filteredImage);
                deleteImage(&greyImage);
                deleteImage(&outputImage);
            }
            
            initImage(&outputImage,originalImage.rows,originalImage.cols,originalImage.depth, originalImage.header);
            initImage(&twistedImage,originalImage.rows,originalImage.cols,originalImage.depth, originalImage.header);
            initImage(&twistedImage1,originalImage.rows,originalImage.cols,originalImage.depth, originalImage.header);
            initImage(&filteredImage,originalImage.rows,originalImage.cols,originalImage.depth, originalImage.header);
            initImage(&greyImage,originalImage.rows,originalImage.cols,1, NULL);
        }
        
        //TODO there is something wrong in image twisting... the C implementation works in the opposite direction of the OpenCL one...
        image_twisting(&originalImage, &twistedImage, TWIST_FACTOR, nThread);
        image_twisting(&twistedImage, &twistedImage1, -TWIST_FACTOR, nThread);
        gaussian_filter(&twistedImage1, &filteredImage, GAUSS_SIGMA, nThread);
        rgb2grey(&filteredImage,&greyImage);
        
        grey2rgb(&outputImage, &greyImage);
        
        //writing file
        //std::cout << "writing back results..." << std::endl;
        if (globalArgs.pathToImages.length() == 0)
            fileName = MAKE_STRING(globalArgs.outputNamePattern << img << "_out.bmp");
        else
            fileName = MAKE_STRING(globalArgs.pathToImages << "/" << globalArgs.imageNamePattern << img << "_out.bmp");
        
        writeImage(fileName.c_str(), &outputImage);
        
        //free allocated memory
        //the original image have to be deleted always since memory is reinstantiated in the load function.
        //Do not delete other objects if they are necessary for other elaborations
        deleteImage(&originalImage);
    }
    
    deleteImage(&twistedImage);
    deleteImage(&twistedImage1);
    deleteImage(&filteredImage);
    deleteImage(&greyImage);
    deleteImage(&outputImage);
    
    // SUPPRESSED OUTPUT
    // std::cout << std::endl;
    
}


/***************************************************/
/*********** End Open MP Implementation ************/
/***************************************************/



/***************************************************/
/************* Open CL Implementation **************/
/***************************************************/

void initOpenCLKernel(OpenCLDevice &device) {
    device.addKernel("rgb2grey");
    device.addKernel("image_twisting");
    device.addKernel("edge_detection");
    device.addKernel("image_downsampling");
    device.addKernel("gaussian_filter");
}


void openCLImplementation(GlobalArgs globalArgs, OpenCLDevice &openCLDevice) {
    sImage originalImage, greyImage, outputImage;
    int rows = 0;
    int cols = 0;
    int depth = 0;
    int img;
    
    std::string fileName;
    
    cl_int status = CL_SUCCESS;
    
    size_t size;
    size_t outSize;
    
    cl_mem inBuffer;
    cl_mem step1Buffer;
    cl_mem step2Buffer;
    cl_mem step3Buffer;
    cl_mem outBuffer;
    
    for(img = globalArgs.startNum; img <= globalArgs.endNum; img++){
        
        if (globalArgs.pathToImages.length() == 0)
            fileName = MAKE_STRING(globalArgs.imageNamePattern << img << ".bmp");
        else
            fileName = MAKE_STRING(globalArgs.pathToImages << "/" << globalArgs.imageNamePattern << img << ".bmp");
        
        std::cout << "Processing " << fileName << std::endl;
        
        readImage(fileName.c_str(), &originalImage);
        
        if(rows!=originalImage.rows || cols!=originalImage.cols || depth!= originalImage.depth){
            
            if(img!=globalArgs.startNum){
                //Delete
                deleteImage(&greyImage);
                deleteImage(&outputImage);
            }
            
            
            rows = originalImage.rows;
            cols = originalImage.cols;
            depth = originalImage.depth;
            
            initImage(&outputImage,originalImage.rows,originalImage.cols,originalImage.depth, originalImage.header);
            initImage(&greyImage,originalImage.rows,originalImage.cols,1, NULL);
            
            size = originalImage.rows*originalImage.cols*sizeof(unsigned char)*originalImage.depth;
            outSize = originalImage.rows*originalImage.cols*sizeof(unsigned char);
            
            inBuffer = clCreateBuffer(openCLDevice.context,CL_MEM_READ_WRITE, size, 0, &status);
            step1Buffer = clCreateBuffer(openCLDevice.context,CL_MEM_READ_WRITE, size, 0, &status);
            step2Buffer = clCreateBuffer(openCLDevice.context,CL_MEM_READ_WRITE, size, 0, &status);
            step3Buffer = clCreateBuffer(openCLDevice.context,CL_MEM_READ_WRITE, size, 0, &status);
            outBuffer = clCreateBuffer(openCLDevice.context,CL_MEM_READ_WRITE, outSize, 0, &status);
            
        }
        
        /*Copy input buffer*/
        status = clEnqueueWriteBuffer(openCLDevice.command_queue, inBuffer, CL_TRUE, 0, size, originalImage.data, 0, NULL, NULL);
        //status = clFlush(openCLDevice.command_queue);
        status = clFinish(openCLDevice.command_queue);
        
        twistFactor(openCLDevice, inBuffer, step1Buffer, rows, cols, depth, TWIST_FACTOR);
        twistFactor(openCLDevice, step1Buffer, step2Buffer, rows, cols, depth, -TWIST_FACTOR);
        gaussian_filter(openCLDevice, step2Buffer, step3Buffer, rows, cols, depth, GAUSS_SIGMA);
        rgb2grey(openCLDevice, step3Buffer, outBuffer, rows, cols);
        
        //status = clEnqueueReadBuffer(openCLDevice.command_queue, step3Buffer, CL_TRUE, 0, size, outputImage.data, 0, NULL, NULL);
        status = clEnqueueReadBuffer(openCLDevice.command_queue, outBuffer, CL_TRUE, 0, outSize, greyImage.data, 0, NULL, NULL);
        status = clFinish(openCLDevice.command_queue);
        
        grey2rgb(&outputImage, &greyImage);
        
        if (globalArgs.pathToImages.length() == 0)
            fileName = MAKE_STRING(globalArgs.outputNamePattern << img << "_out.bmp");
        else
            fileName = MAKE_STRING(globalArgs.pathToImages << "/" << globalArgs.imageNamePattern << img << "_out.bmp");
        
        writeImage(fileName.c_str(), &outputImage);
        
        deleteImage(&originalImage);
        
    }
    
    deleteImage(&greyImage);
    deleteImage(&outputImage);
    
}


/***************************************************/
/*********** End Open CL Implementation ************/
/***************************************************/


int main(int argc, char** argv){
    
    int i;
    
    parseCommmand(argc,argv);
    
    OpenCLDevice deviceCPU;
    if (globalArgs.target == OPENCL_CPU) {
        std::cout << "Init CPU device" << std::endl;
        deviceCPU = OpenCLDevice(CL_DEVICE_TYPE_CPU);
        initOpenCLKernel(deviceCPU);
    }
    
    std::cout << "Init GPU device" << std::endl;
    OpenCLDevice deviceGPU = OpenCLDevice(CL_DEVICE_TYPE_GPU);
    initOpenCLKernel(deviceGPU);
    
    
    /*
    unsigned long t0CPU, t1CPU, tCPU;
    unsigned long t0GPU, t1GPU, tGPU;
    
    t0CPU = getTime();
    //Correlation_CPU_2D(corrInput, corrInputStdDev, corrOutput);
    t1CPU = getTime();
    tCPU = t1CPU - t0CPU;
    
    std::cout << "CPU time: " << tCPU << std::endl;
    
    
    t0GPU = getTime();
    openCLImplementation(globalArgs, deviceGPU);
    t1GPU = getTime();
    tGPU = t1GPU - t0GPU;
    
    std::cout << "GPU time: " << tGPU << std::endl;
    */
    
    AppPerformanceMonitor * performanceMonitor = new AppPerformanceMonitor("test");
    AppScheduleMonitor * schedulerMonitor = new AppScheduleMonitor("test");
    
    ReportMonitor report;

    
    /*CPU Implementation*/
    ImplementationType cpuImpl = [&] () -> void {
        std::cout << "CPU" << std::endl;
        
        if (globalArgs.target == OPENCL_CPU) {
            openCLImplementation(globalArgs, deviceCPU);
        } else {
            openMPImplementation(globalArgs, schedulerMonitor->getNumberOfThread());
        }
    };
    
    /*GPU Implementation*/
    ImplementationType gpuImpl = [&] () -> void {
        std::cout << "GPU" << std::endl;
        openCLImplementation(globalArgs, deviceGPU);
    };
    
    
    schedulerMonitor->registerImplementation(cpuImpl,CPUScheduleType);
    schedulerMonitor->registerImplementation(gpuImpl,GPUScheduleType);
    schedulerMonitor->setFastestImplementation(CPUScheduleType);
    schedulerMonitor->setSlowestImplementation(GPUScheduleType);
    
    
    ApplicationGoal applicationGoal;
    applicationGoal.trhoughput = 1.4;
    
    ApplicationConstraints applicationConstraint;
    applicationConstraint.trhoughputMin = 0.7;
    
    performanceMonitor->setApplicationGoal(applicationGoal);
    performanceMonitor->setApplicationConstraints(applicationConstraint);
    
    schedulerMonitor->setHasToBeProfiled(true);
    
    
    bool performanceMonitorRegistrated = performanceMonitor->registerToOrchestrator();
    if (!performanceMonitorRegistrated) {
        delete performanceMonitor;
    }
    
    bool schedulerMonitorRegistrated = schedulerMonitor->registerToOrchestrator();
    if (!schedulerMonitorRegistrated) {
        delete schedulerMonitor;
    }
    
    


    report.startTimer();

    
    for (i = 0; i<50; i++) {
        schedulerMonitor->executeHeterogenousImplementations();
        performanceMonitor->incrementHeartbeat(1);
        report.addIterationData(schedulerMonitor->getChosenScheduleType(), performanceMonitor->averageThroughput(), performanceMonitor->getHeartbeat(), schedulerMonitor->getNumberOfThread());
    }
    
    report.stopTimer();

    
    bool performanceMonitorDeregistrated = performanceMonitor->deregisterToOrchestrator();
    if (performanceMonitorDeregistrated) {
        delete performanceMonitor;
    }
    
    bool schedulerMonitorDeregistrated = schedulerMonitor->deregisterToOrchestrator();
    if (schedulerMonitorDeregistrated) {
        delete schedulerMonitor;
    }
    
    report.reportOnDisk();

    
    return 0;
    
}
