//
//  main.cpp
//  SampleApp
//
//  Created by Raegal on 16/03/15.
//  Copyright (c) 2015 ingconti.com. All rights reserved.
//

#include <unistd.h>
#include <time.h>
#include <functional>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <getopt.h>
#include <sys/time.h>

#include <libThreadScale.hpp>

#include <OpenMPImageProcessingKernels.hpp>

#include <iostream>

#define TWIST_FACTOR 0.9
#define GAUSS_SIGMA 2.5

#define ROWS 1020
#define COLS 1980
#define BPP 3

typedef struct globalArgs_t{
    std::string imageNamePattern;
    std::string outputNamePattern;
    std::string pathToImages;
    unsigned int startNum;
    unsigned int endNum;
    unsigned int threads;
    double goal;
    unsigned int iterations;
} GlobalArgs;


static const char *optString = "i:o:s:e:p:n:g:r:";

static struct option longOpts[] = {
    { "imageNamePattern", required_argument, NULL, 'i' },
    { "outputNamePattern", required_argument, NULL, 'o' },
    { "startNum", required_argument, NULL, 's' },
    { "endNum", required_argument, NULL, 'e' },
    { "pathToImages", required_argument, NULL, 'p' },
    { "threads", required_argument, NULL, 'n' },
    { "goal", required_argument, NULL, 'g' },
    { "iterations", required_argument, NULL, 'r' },
    { 0, 0, 0, 0 }
};

GlobalArgs globalArgs;

void parseCommmand(int argc, char** argv){
    int longIndex;
    int opt;
    
    while((opt = getopt_long_only(argc, argv, optString, longOpts, &longIndex)) != -1 ) {
        switch( opt ) {
            case 'i':
                globalArgs.imageNamePattern = optarg;
                break;
                
            case 'o':
                globalArgs.outputNamePattern = optarg;
                break;
                
            case 's':
                globalArgs.startNum = std::atoi(optarg);
                break;
                
            case 'e':
                globalArgs.endNum = std::atoi(optarg);
                break;
                
            case 'p':
                globalArgs.pathToImages = optarg;
                break;

            case 'n':
                globalArgs.threads = atoi(optarg);
                break;

            case 'g':
                globalArgs.goal = atof(optarg);
                break;
            case 'r':
                globalArgs.iterations = atoi(optarg);
                break;
        }
    }
}


/***************************************************/
/************* Open MP Implementation **************/
/***************************************************/

void backgroundSubtraction(GlobalArgs globalArgs, unsigned int nThread, sImage backgroundImage, sImage frameImage, sImage outputImage){

    unsigned int diff = 0;

#pragma omp parallel for firstprivate(outputImage)
    for(int i=0; i<backgroundImage.rows * backgroundImage.cols * backgroundImage.depth; i++){
        outputImage.data[i] = backgroundImage.data[i] - frameImage.data[i];
        if(outputImage.data[i] != 0)
            __sync_fetch_and_add(&diff, 1);
    }

}


/***************************************************/
/*********** End Open MP Implementation ************/
/***************************************************/

int main(int argc, char** argv){
    
    int i;

    std::cout << "Parsing input" << std::endl;

    parseCommmand(argc,argv);
    
    std::cout << "Setting up library" << std::endl;

    LibThreadScale libQoS;
    libQoS.setMinQoS(globalArgs.goal);
    libQoS.setForceThreads(globalArgs.threads);

    std::cout << "Dumping data" << std::endl;

    std::cout << globalArgs.threads << std::endl;
    std::cout << globalArgs.imageNamePattern << std::endl;
    std::cout << globalArgs.pathToImages<< std::endl;
    std::cout << globalArgs.startNum << std::endl;
    std::cout << globalArgs.endNum << std::endl;
    std::cout << globalArgs.iterations << std::endl;

    std::cout << "Running" << std::endl;

    sImage backgroundImage, currentImage, outputImage;

    initImage(&backgroundImage,ROWS,COLS,BPP, NULL);
    initImage(&currentImage,ROWS,COLS,BPP, NULL);
    initImage(&outputImage,ROWS,COLS,BPP, NULL);

    for (i = 0; i<globalArgs.iterations; i++) {
        backgroundSubtraction(globalArgs, 0, backgroundImage, currentImage, outputImage);
        libQoS.sendHeartbeat(1);
    }

    deleteImage(&backgroundImage);
    deleteImage(&currentImage);
    deleteImage(&outputImage);

    libQoS.dumpInfo("background");
    
    return 0;
    
}
